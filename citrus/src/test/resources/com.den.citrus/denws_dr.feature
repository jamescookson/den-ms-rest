Feature: dr-rates endpoints

  Background:
    Given variables
      | sStatus  | UP       |
      | statUp   | UP       |
      | statDn   | DOWN     |
      | sUrl     | /schedules |
      | pgString | program-name=|
      | pgName   | Arabesque    |
      | fmString | format=      |
      | fmType   | flattened-rate|
      | frStr    | from=         |
      | toStr    | to=           |
      | intStr   | 15            |
      | tS    | 1538776564000    |
      | tS2   | ${tS}+900000     |
      | tS3   | ${tS}+1800000    |
      | frDate   | 2018-10-05T21:56:04|
      | toDate   | 2018-11-05T23:56:04|
      | schId    | 9bc3d637-46ca-426f-9478-ab4e0cc9dbf4|
      | schIdSoak | f024f5f8-6454-4f8a-8340-efe90b297e0c |
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |

  @regression @soak
  Scenario: API: dr-rates by program name
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}
    Then validate $[0].drScheduleId is ${schId}
    Then validate $[0].programName is ${pgName}
    Then validate $[0].startDateTimeUtc is ${frDate}
    Then validate $[0].endDateTimeUtc is ${toDate}
    Then receives status 200 OK

  @regression
  Scenario: API: flattened dr-rates by ID
    Given http-client "dr_adapter"
    When sends GET ${sUrl}/${schId}?${fmString}${fmType}
    Then validate $.drScheduleId is ${schId}
    Then validate $.programName is ${pgName}
    Then validate $.intervalMinutes is ${intStr}
    Then validate $.timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name, scheduled range AND flattened output
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then validate $[0].programName is ${pgName}
    Then validate $[0].intervalMinutes is ${intStr}
    Then validate $[0].timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK

  @regression
  Scenario Outline: <descr>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends GET <url>
    And receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | descr |
      | dr    | ${sUrl}  | 200 | OK  | available schedules |
      | dr    | ${sUrl}/${schId}    | 200  | OK  | dr-rates by id |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${toDate}  | 200  | OK  | dr-rates by name and scheduled range |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}  | 200  | OK  | dr-rates by name and scheduled range (missing TO) |
      | dr    | ${sUrl}?${pgString}${pgName}&${toStr}${toDate}    | 200  | OK  |  by name and scheduled range (missing FROM) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2000-10-05T21:56:04&${toStr}${toDate}  | 200  | OK  | dr-rates by name and scheduled range (custom FROM) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}2024-10-05T21:56:04  | 200  | OK  |  by name and scheduled range (custom TO) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badTxt}    | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badChr}  | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${badChr}&${toStr}${toDate}  | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${badTxt}&${toStr}${toDate}    | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | /${badTxt}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${badChr}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${ise500}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${badChr}?${pgString}${pgName}  | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | /${badTxt}?${pgString}${pgName}  | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/${badChr}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${badTxt}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${ise500}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | /${badChr}/${schId}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | /${badTxt}/${schId}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/${schId}?${fmString}${badChr} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${schId}?${fmString}${badTxt} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${schId}?${fmString} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | //${schId}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/?${fmString} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2020-01-01T01:01:01+000Z&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}01/01/2020&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2000-01-01T01:01:01+000Z&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}01/01/2000&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |

  @soak
  Scenario Outline: <descr>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends GET <url>
    And receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | descr |
      | dr    | ${sUrl}  | 200 | OK  | available schedules |
      | dr    | ${sUrl}/${schIdSoak}    | 200  | OK  | dr-rates by id |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${toDate}  | 200  | OK  | dr-rates by name and scheduled range |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}  | 200  | OK  | dr-rates by name and scheduled range (missing TO) |
      | dr    | ${sUrl}?${pgString}${pgName}&${toStr}${toDate}    | 200  | OK  |  by name and scheduled range (missing FROM) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2000-10-05T21:56:04&${toStr}${toDate}  | 200  | OK  | dr-rates by name and scheduled range (custom FROM) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}2024-10-05T21:56:04  | 200  | OK  |  by name and scheduled range (custom TO) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badTxt}    | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badChr}  | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${badChr}&${toStr}${toDate}  | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}${badTxt}&${toStr}${toDate}    | 400  | BAD_REQUEST  | test - expected failure (400) |
      | dr    | /${badTxt}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${badChr}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${ise500}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}  | 404 | NOT_FOUND  | test - expected failure (404) |
      | dr    | /${badChr}?${pgString}${pgName}  | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | /${badTxt}?${pgString}${pgName}  | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/${badChr}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${badTxt}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${ise500}?${fmString}${fmType}  | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | /${badChr}/${schIdSoak}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | /${badTxt}/${schIdSoak}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/${schIdSoak}?${fmString}${badChr} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${schIdSoak}?${fmString}${badTxt} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}/${schIdSoak}?${fmString} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | //${schId}?${fmString}${fmType} | 404 | NOT_FOUND | test - expected failure (404) |
      | dr    | ${sUrl}/?${fmString} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2020-01-01T01:01:01+000Z&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}01/01/2020&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}2000-01-01T01:01:01+000Z&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |
      | dr    | ${sUrl}?${pgString}${pgName}&${frStr}01/01/2000&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST | test - expected failure (400) |

  @regression
  Scenario: API: flattened dr-rates by ID
    Given http-client "dr_adapter"
    When sends GET ${sUrl}/${schId}?${fmString}${fmType}
    Then validate $.drScheduleId is ${schId}
    Then validate $.programName is ${pgName}
    Then validate $.timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name, scheduled range AND flattened output
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then validate $[0].programName is ${pgName}
    Then validate $[0].intervalMinutes is ${intStr}
    Then validate $[0].timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK

