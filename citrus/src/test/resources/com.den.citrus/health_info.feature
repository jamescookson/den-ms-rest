Feature:  Health|Info

  Background:
    Given variables
      | sStatus | UP |
      | svcState | /health |
      | svcInfo  | /info   |

  @regression
  Scenario Outline: oe-service health, with deeper state calls
    Given http-client "oe_adapter"
    Given variable "passEnv" is "dev"
    And Accept: application/json
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then validate $.diskSpace.status is ${sStatus}
    Then validate $.configServer.status is ${sStatus}
    And receives status 200 OK

  @regression
  Scenario Outline: forecastms-service health, with deeper state calls
    Given http-client "forecastms_adapter"
    Given variable "passEnv" is "dev"
    And Accept: application/json
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then validate $.db.status is ${sStatus}
    And receives status 200 OK

  @regression
  Scenario Outline: <adapt>-service health
    Given http-client "<adapt>_adapter"
    Given variable "passEnv" is "dev"
    And Accept: application/json
    When sends GET <url>
    Then validate $.status is <stats>
    And receives status <code> <msg>

    Examples:
      | adapt         | url         | stats      | code | msg |
      | forecastms    | ${svcState} | ${sStatus} | 200  | OK  |
      | capture       | ${svcState} | ${sStatus} | 200  | OK  |
      | eventrecorder | ${svcState} | ${sStatus} | 200  | OK  |
      | dr            | ${svcState} | ${sStatus} | 200  | OK  |
      | topo          | ${svcState} | ${sStatus} | 200  | OK  |
      | dayahead      | ${svcState} | ${sStatus} | 200  | OK  |
      | tornado       | ${svcState} | ${sStatus} | 200  | OK  |
      | carnac        | ${svcState} | ${sStatus} | 200  | OK  |
      | vpp           | ${svcState} | ${sStatus} | 200  | OK  |
      | rate          | ${svcState} | ${sStatus} | 200  | OK  |
      | forecast      | ${svcState} | ${sStatus} | 200  | OK  |
      | billCalc      | ${svcState} | ${sStatus} | 200  | OK  |
      | weather      | ${svcState} | ${sStatus} | 200  | OK  |
      | derp      | ${svcState} | ${sStatus} | 200  | OK  |
      | eventsvc      | ${svcState} | ${sStatus} | 200  | OK  |
      | senderp      | ${svcState} | ${sStatus} | 200  | OK  |
      | premo      | ${svcState} | ${sStatus} | 200  | OK  |

  @regression
  Scenario Outline: <adapt>-service info
    Given http-client "<adapt>_adapter"
    Given variable "passEnv" is "dev"
    And Accept: application/json
    When sends GET <url>
    Then receives status <code> <msg>

    Examples:
      | adapt         | url        | code | msg |
      | capture       | ${svcInfo} | 200  | OK  |
      | eventrecorder | ${svcInfo} | 200  | OK  |
      | dr            | ${svcInfo} | 200  | OK  |
      | topo          | ${svcInfo} | 200  | OK  |
      | dayahead      | ${svcInfo} | 200  | OK  |
      | tornado       | ${svcInfo} | 200  | OK  |
      | carnac        | ${svcInfo} | 200  | OK  |
      | vpp           | ${svcInfo} | 200  | OK  |
      | rate          | ${svcInfo} | 200  | OK  |
      | forecast      | ${svcInfo} | 200  | OK  |
      | stats         | ${svcInfo} | 200  | OK  |
      | oe            | ${svcInfo} | 200  | OK  |
      | billCalc      | ${svcInfo} | 200  | OK  |
      | weather      | ${svcInfo} | 200  | OK  |
      | derp      | ${svcInfo} | 200  | OK  |
      | eventsvc      | ${svcInfo} | 200  | OK  |
      | senderp      | ${svcInfo} | 200  | OK  |
