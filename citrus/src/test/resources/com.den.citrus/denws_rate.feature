Feature:  rate-svc endpoints

  Background:
    Given variables
      | uRest   | /rest |
      | uSite   | /sites  |
      | uStore  | /stores |
      | uRate   | /rates  |
      | rstUrl  | /rest   |
      | metUrl  | /meta   |
      | cfgUrl  | /config |
      | tarUrl  | /tariffs  |
      | sUrl    | /schedules|
      | uAcct   | /accounts |
      | vUrl    | /version  |
      | utlUrl  | /utilities|
      | dayUrl  | /dayahead |
      | delUrl  | /delivery |
      | tariffCde|  CSTE19  |
      | gString  | group_id |
      | tString  | tariffId |
      | utlString| utilityId|
      | zString  | zipcode  |
      | qeSiteNm | TestSite1  |
      | qeStorNm | Battery1   |
      | qeRak1Nm | ESS Rack 1 |
      | qeRak2Nm | ESS Rack 2 |
      | tarCde   | E19      |
      | zipCde   | 98466    |
      | tarId    | 3161243  |
      | ratCde   | CSTE19   |
      | charStr  | %21%7E%40%5B%2e%23%5D%28%5F%24%29%2A%3A |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |
      | dateStr  | 2018-11-05T23:56:04|
      | charStr2 | %7B%5B%E2%80%A6?%5D%7D |
      | textStr  | qwertyuiopasdfghjkl |
      | utilId   | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | qeStore | 768b8574-d513-4b48-a5cc-1401808078ea  |
      | qeDvcId | 672cb9a6-b742-4c95-8388-648c9358f771  |
      | qeStrId | 88b2c7a1-435c-45b2-8a74-20653aac221a  |
      | qeRak1  | a6d41e60-680e-4dcd-8ce0-d04d69d7add4  |
      | qeRak2  | 952e8e00-7b34-44ca-ba24-fd0b09fe3a24  |
      | qeStoreSiteId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
      | bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
      | longStr  | 012345678100123456781001234567810012345678100123456785001234567810012345678100123456781001234567810012345671000123456781001234567810012345678100123456781001234567850012345678100123456781001234567810012345678100123456720001234567810012345678100123456781001234567810012345678500123456781001234567810012345678100123456781001234567300 |

  @regression
  Scenario: V : rate-capture-svc endpoint - specific ID, XML retuened : tariffs/<ID>
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
    When sends GET ${tarUrl}/${tarId}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoint - all configs : tariffs/configs
    Given http-client "capture_adapter"
    When sends GET ${tarUrl}/configs
    Then receives status 200 OK

  @regression
  Scenario: V: rate-svc plan by ZipCode : Info specific to a known rate
    Given http-client "rate_adapter"
    Given variables
      | zIp     | 98406 |
      | rCode   | CNFL_Supply_usd |
      | rName   | CNFL_TMT_Supply US$ |
      | uName   | Instituto Costarricense de Electricidad |
      | uId     | 68913276-27be-4387-bb66-c690899c7546    |
      | tType   | Supply                                  |
      | eXp     | never                                   |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}?${zString}=${zIp}
    Then validate $[0].code is ${rCode}
    Then validate $[0].name is ${rName}
    Then validate $[0].utilityName is ${uName}
    Then validate $[0].utilityId is ${uId}
    Then validate $[0].tariffType is ${tType}
    Then validate $[0].expires is ${eXp}
    Then receives status 200 OK

  @requiresfix
  Scenario: V : rate-capture-svc endpoints : tariffs/<ID>/rates : Specific data
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | tName  | Residential |
      | tCode  | None_Residential |
      | rGroup1 | Customer Charge |
      | rGroup2 | Minimum Charge |
      | rGroup3 | Energy Charge |
      | rChgType1 | FIXED_PRICE |
      | rChgType2 | MINIMUM |
      | rChgType3 | CONSUMPTION_BASED |
      | predict1  | fixedMonthly      |
      | predict2  | Ignore            |
      | predict3  | MonthlyEnergyRates|
      | season    | Summer            |
      | sStrtEnd  | 4/1 - 9/11        |
    When sends GET ${tarUrl}/${tarId}${uRate}
    Then validate $.masterTariffId is ${mTarId}
    Then validate $.tariffName is ${tName}
    Then validate $.tariffCode is ${tCode}
    Then validate $.rates[0].rateGroupName is ${rGroup1}
    Then validate $.rates[0].rateName is ${rGroup1}
    Then validate $.rates[0].chargeType is ${rChgType1}
    Then validate $.rates[0].prediction is ${predict1}
    Then validate $.rates[0].seasonName is ${season}
    Then validate $.rates[0].seasonStartEnd is ${sStrtEnd}
    Then validate $.rates[1].rateGroupName is ${rGroup2}
    Then validate $.rates[1].rateName is ${rGroup2}
    Then validate $.rates[1].chargeType is ${rChgType2}
    Then validate $.rates[1].prediction is ${predict2}
    Then validate $.rates[1].seasonName is ${season}
    Then validate $.rates[1].seasonStartEnd is ${sStrtEnd}
    Then validate $.rates[2].rateGroupName is ${rGroup3}
    Then validate $.rates[2].rateName is ${rGroup3}
    Then validate $.rates[2].chargeType is ${rChgType3}
    Then validate $.rates[2].prediction is ${predict3}
    Then validate $.rates[2].seasonName is ${season}
    Then validate $.rates[2].seasonStartEnd is ${sStrtEnd}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoints : tariffs/<ID>/config : specific config
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
    When sends GET ${tarUrl}/${tarId}${cfgUrl}
    Then validate $.masterTariffId is ${mTarId}
    Then validate $.code is ${tCode}
    Then validate $.lseId is ${lseId}
    Then validate $.utility is ${uTil}
    Then validate $.smart-meter-interval is 12
    Then validate $.demand-calc-interval is 2
    Then sends GET ${tarUrl}/${tarId}/autoconfig
    Then receives status 200 OK

  @regression
  Scenario Outline: <act> rate-capture tarrif w/ flavor - <code> <msg>
    Given http-client "<adapt>_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
      | flavDesignate1  | Service%20Type  |
      | flavDesignate2  | Territory     |
      | flavDesignate3  | Metering%20Service%20Charge  |
      | flav1  | High%20Tension     |
      | flav2  | Low%20Tension      |
      | flav3  | true   |
      | flav4  | false  |
      | flav5  | Zone%20H           |
      | flav6  | Zone%20I           |
      | flav7  | Zone%20J           |
      | requestFlavor1 | ${flavDesignate1}-${flav1} |
      | requestFlavor2 | ${flavDesignate1}-${flav2} |
      | requestFlavor3 | ${flavDesignate2}-${flav3} |
      | requestFlavor4 | ${flavDesignate2}-${flav4} |
      | requestFlavor5 | ${flavDesignate3}-${flav5} |
      | requestFlavor6 | ${flavDesignate3}-${flav6} |
      | requestFlavor7 | ${flavDesignate3}-${flav7} |
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor1} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor2} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor3} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor4} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor5} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor6} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor7} | 200 | OK |

  @regression
  Scenario Outline: <act> rate-capture-svc endpoint - flavor based : tariffs/id/flavor_designate - <code> <msg>
    Given http-client "<adapt>_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
      | flavDesignate1  | Service%20Type  |
      | flavDesignate2  | Territory     |
      | flavDesignate3  | Metering%20Service%20Charge  |
      | flav1  | High%20Tension     |
      | flav2  | Low%20Tension      |
      | flav3  | true   |
      | flav4  | false  |
      | flav5  | Zone%20H           |
      | flav6  | Zone%20I           |
      | flav7  | Zone%20J           |
      | requestFlavor0 | ${flav1}-${flavDesignate3} |
      | requestFlavor8 | ${flavDesignate3}-${flav7} |
      | requestFlavor9 | ${flavDesignate1}-${flav5} |
      | requestFlavorA | ${flavDesignate2}-${flav4} |
      | requestFlavorB | ${flavDesignate1}-${flav6} |
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor8} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor9} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavorA} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavorB} | 200 | OK |
      | capture | GET | ${tarUrl}/${rateId}/${requestFlavor0} | 200 | OK |

  # specific to master rate ID 3161243, returns custom made rate, direct verification to follow
  @regression
  Scenario: V : rate-capture-svc endpoint - raw data for available configs on <ID> : tariffs/<ID>/raw
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3306911 |
      | raW    | /raw    |
    When sends GET ${tarUrl}/${mTarId}${raW}
    Then validate $.type is Tariff
    Then validate $.results[0].tariffId is ${rateId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoint - raw data for 3306911 - Town of Ruston
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3306911 |
      | raW    | /raw    |
    When sends GET ${tarUrl}/${mTarId}${raW}
    Then validate $.type is Tariff
    Then validate $.results[0].tariffId is ${rateId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then validate $.results[0].tariffCode is None
    Then validate $.results[0].tariffName is Residential
    Then validate $.results[0].lseId is 1043
    Then validate $.results[0].lseName is Town of Ruston
    Then validate $.results[0].priorTariffId is ${mTarId}
    Then validate $.results[0].tariffType is DEFAULT
    Then validate $.results[0].customerClass is RESIDENTIAL
    Then validate $.results[0].customerCount is 494
    Then validate $.results[0].territoryId is 1151
    Then validate $.results[0].effectiveDate is 2018-04-01
    Then validate $.results[0].timeZone is US/Pacific
    Then validate $.results[0].billingPeriod is MONTHLY
    Then validate $.results[0].currency is USD
    Then validate $.results[0].chargeTypes is FIXED_PRICE,CONSUMPTION_BASED,MINIMUM
    Then validate $.results[0].chargePeriod is MONTHLY
    Then validate $.results[0].hasTimeOfUseRates is false
    Then validate $.results[0].hasTieredRates is false
    Then validate $.results[0].hasContractedRates is false
    Then validate $.results[0].hasRateApplicability is false
    Then validate $.results[0].isActive is true
    Then receives status 200 OK

  @regression
  Scenario: Validate rate-service schedules API endpoint
    Given http-client "rate_adapter"
    When sends GET ${sUrl}
    Then receives status 200 OK

  @regression
  Scenario: Validate rate-service schedules by code API endpoint
    Given http-client "rate_adapter"
    When sends GET ${sUrl}/${ratCde}
    Then receives status 200 OK

  @regression
  Scenario: Validate topo-service utilities
    Given http-client "rate_adapter"
    When sends GET ${utlUrl}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by tariff id only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${tString}=${tarId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by utility id only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${utlString}=${utilId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code & utility id
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${utlString}=${utilId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code & tariff id
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${tString}=${tarId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code, utility & tariff id's
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${utlString}=${utilId}&${tString}=${tarId}
    Then receives status 200 OK

  @regression @parispostalcode
  Scenario Outline: <act> rate-service tarrif w/ zipcode querystring - <code> <msg> >> forgiveness tst - currently passing w/ 200 , eventually expect 400/404
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    And Header Authorization: ${bAuth}
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg |
      | rate  | GET | ${tarUrl}?${zString}=${longStr} | 200 | OK |
      | rate  | GET | ${tarUrl}?${zString}=${textStr} | 200 | OK |
      | rate  | GET | ${tarUrl}?${zString}=${charStr} | 200 | OK |
      | rate  | GET | ${tarUrl}?${zString}=${charStr2} | 200 | OK |
      | rate  | GET | ${tarUrl}?${zString}=75008 | 200 | OK |

  @regression @eufuelcelltariff
  Scenario Outline: <act> rate-service tarrif w/ tariffid querystring - <code> <msg> >> forgiveness tst - currently passing w/ 200 , eventually expect 400/404
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    And Header Authorization: ${bAuth}
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg |
      | rate  | GET | ${tarUrl}?${tString}=${longStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${tString}=${textStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${tString}=${charStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${tString}=${charStr2} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${tString}=8501620030 | 200  | OK  |

  @regression @mockFRcode
  Scenario Outline: <act> rate-service tarrif w/ utilityId querystring - <code> <msg> >> forgiveness tst - currently passing w/ 200 , eventually expect 400/404
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    And Header Authorization: ${bAuth}
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg |
      | rate  | GET | ${tarUrl}?${utlString}=${longStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${utlString}=${textStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${utlString}=${charStr} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${utlString}=${charStr2} | 200  | OK  |
      | rate  | GET | ${tarUrl}?${utlString}=FRA123456 | 200  | OK  |

  @regression
  Scenario Outline: <act> rate-service tarrif w/ querystring entries - failures - <code> <msg> - <note>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    And Header Authorization: ${bAuth}
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | act | url | code | msg | note |
      | rate | GET | ${tarUrl}?${zString}=${ise500} | 400 | BAD_REQUEST | zipCode |
      | rate | GET | ${tarUrl}?${tString}=${ise500} | 400 | BAD_REQUEST | tariffId |
      | rate | GET | ${tarUrl}?${utlString}=${ise500} | 400 | BAD_REQUEST | utilityId |

  @regression
  Scenario: V: rate-tariffs-meta
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${metUrl}/${tarCde}
    Then validate $.code is ${tarCde}
    Then validate $.deleted is false
    Then validate $.tariffType is Delivery
    Then validate $.utilityName is Pacific Gas & Electric Co
    Then validate $.name is PGE E19
    Then receives status 200 OK

  @regression
  Scenario: V: rate-tariffs-dayahead --> need dateTime,start,end qstring
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${dayUrl}/${tarCde}
    Then validate $[0].code is ${tarCde}
    Then validate $[0].interval is PT1H
    Then receives status 200 OK

  @regression
  Scenario: V: rate-tariffs-dayahead-missingdays --> need zone,start,end qstring
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=UTC
    Then receives status 200 OK

  @regression @expected_failure @den2018-3768
  Scenario Outline: <act> rate-tariffs-dayahead-missingdays - failures - <code> <msg> - <note>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    And Header Authorization: ${bAuth}
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | act | adapt | url | code | msg | note |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2020-12-31&zone=EST | 400 | BAD_REQUEST | future-date |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=31-01-2018&zone=EST | 400 | BAD_REQUEST | date-format |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-01-10T00:00:01Z&zone=EST | 400 | BAD_REQUEST | iso8601format-start |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=01.01.2018&zone=EST | 400 | BAD_REQUEST | date-format |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?start=2018-09-10&zone=UTC | 400 | BAD_REQUEST | missing-code |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=111&start=01.01.2018&zone=EST | 400 | BAD_REQUEST | bad-code |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=4095D&start=01.01.2018&zone=EST | 400 | BAD_REQUEST | bad-code |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=GOMER&start=01.01.2018&zone=EST | 400 | BAD_REQUEST | bad-code |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=g0M3Rr&start=01.01.2018&zone=EST | 400 | BAD_REQUEST | bad-code |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=XXX | 500 | INTERNAL_SERVER_ERROR | bad-zone |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=%5E | 500 | INTERNAL_SERVER_ERROR | bad-zone-char |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=utc | 500 | INTERNAL_SERVER_ERROR | bad-zone |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=-25:00 | 500 | INTERNAL_SERVER_ERROR | timeVal-for-zone |
      | GET | rate | ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=-01:02.025 | 500 | INTERNAL_SERVER_ERROR | timedateal-bad-zone |

  @regression
  Scenario: V: SD G&E rate-service tarrif by zip+utility (very specific : used for DR)
    Given http-client "rate_adapter"
    Given variables
      | zipCde | 94111 |
      | utilId | 77e5bbb2-74cc-4f84-86af-51b0d56b43bf |
      | tTypeA | Delivery |
      | tTypeB | Supply |
      | uCodeA  | SDGE_AL-TOU_Secondary-Inside |
      | fullNameA | AL-TOU Secondary Inside City Limits |
      | tariffA   | 81587 |
      | uCodeB | DGR_Secondary-Inside |
      | fullNameB | DG-R Secondary Inside City Limits - Solar-Only (UNVERIFIED) |
      | tariffB   | 3154259 |
      | utilName | San Diego Gas & Electric |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}?${zString}=${zipCde}&${utlString}=${utilId}
    Then validate $[1].code is ${uCodeA}
    Then validate $[1].name is ${fullNameA}
    Then validate $[1].utilityName is ${utilName}
    Then validate $[1].utilityId is ${utilId}
    Then validate $[1].tariffType is ${tTypeA}
    Then validate $[0].utilityName is ${utilName}
    Then validate $[0].utilityName is ${utilName}
    Then validate $[0].tariffType is ${tTypeA}
    Then validate $[0].code is ${uCodeB}
    Then validate $[0].name is ${fullNameB}
    Then receives status 200 OK

  @regression
  Scenario: V: SD G&E rate-service delivery info, specific rate (AL-TOU)
    Given http-client "rate_adapter"
    Given variables
      | zipCde | 94111 |
      | utilId | 77e5bbb2-74cc-4f84-86af-51b0d56b43bf |
      | tTypeA | Delivery |
      | tTypeB | Supply |
      | uCodeA  | SDGE_AL-TOU_Secondary-Inside |
      | fullNameA | AL-TOU Secondary Inside City Limits |
      | tariffA   | 81587 |
      | utilName | San Diego Gas & Electric |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    Then sends GET ${tarUrl}${delUrl}/${uCodeA}
    Then validate $.code is ${uCodeA}
    Then validate $.name is ${fullNameA}
    Then validate $.utilityId is ${utilId}
    Then validate $.utilityName is ${utilName}
    Then validate $.demandCalcIntervals is 4
    Then validate $.smartMeterIntervals is 12
    Then validate $.monthlyDeliveryRates.rate[0].name is Maximum On-Peak Demand  - Summer
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[0].rate is 9.77
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[1].rate is 11.3
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[2].rate is 11.03
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[4].rate is 9.83
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[6].rate is 9.92
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[7].rate is 10.88
    Then validate $.monthlyDeliveryRates.rate[0].rateDateRanges[8].rate is 10.99
    Then validate $.monthlyDeliveryRates.rate[0].timePeriods[0].timeRange.start.milliseconds is 57660000
    Then receives status 200 OK

  @regression
  Scenario: verify SDG&E rate-service schedule info, specific rate (AL-TOU)
    Given http-client "rate_adapter"
    Given variables
      | zipCde | 94111 |
      | utilId | 77e5bbb2-74cc-4f84-86af-51b0d56b43bf |
      | tTypeA | Delivery |
      | uCodeA  | SDGE_AL-TOU_Secondary-Inside |
      | fullNameA | AL-TOU Secondary Inside City Limits |
      | tariffA   | 81587 |
      | utilName | San Diego Gas & Electric |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    Then sends GET ${sUrl}/${uCodeA}
    Then validate $.code is ${uCodeA}
    Then validate $.name is ${fullNameA}
    Then validate $.tariffId is ${tariffA}
    Then validate $.frequency is P15D
    Then validate $.timeZone is US/Pacific
    Then receives status 200 OK

  @regression
  Scenario Outline: Failure cases - Update delivery|supply info, <code> <msg> - <note>
    Given http-client "denws_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | delBefor | <del1> |
      | delAfter | <del2> |
      | supBefor | <sup1> |
      | supAfter | <sup2> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${delAfter},
      "deliveryBefore":${delBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":${supAfter}
    }
    """
    When sends PUT /rest/sites/${bSiteId}/billing/rates
    Then receives status <code> <msg>

    Examples:
      | del1 | del2 | sup1 | sup2 | code | msg | note |
      | 123456 | SDGE_AL-TOU_Secondary-Inside | null | null | 400  | BAD_REQUEST | del-SDGE_ALTOU |
      | true | SDGE_AL-TOU_Secondary-Inside | null | null | 400  | BAD_REQUEST | del-SDGE_ALTOU |
      | SDGE_AL-TOU_Secondary-Inside | 123456 | null | null | 400  | BAD_REQUEST | del-SDGE_ALTOU |
      | SDGE_AL-TOU_Secondary-Inside | true | null | null | 400  | BAD_REQUEST | del-SDGE_ALTOU |
      | SDGE_AL-TOU_Secondary-Inside | SDGE_AL-TOU_Secondary-Inside | 123456 | flat105 | 400  | BAD_REQUEST | sup-flat05 |
      | SDGE_AL-TOU_Secondary-Inside | SDGE_AL-TOU_Secondary-Inside | true | flat105 | 400  | BAD_REQUEST | sup-flat05 |
      | SDGE_AL-TOU_Secondary-Inside | SDGE_AL-TOU_Secondary-Inside | flat105 | 123456 | 400  | BAD_REQUEST | sup-flat05 |
      | SDGE_AL-TOU_Secondary-Inside | SDGE_AL-TOU_Secondary-Inside | flat105 | true | 400  | BAD_REQUEST | sup-flat05 |

  @regression
  Scenario Outline: Updating site info, failure cases, <code> <msg>
    Given http-client "denws_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | siteName | BldgDER+Solar_DONOTREMOVE |
      | newId | ${bSiteId} |
      | siteAddress | <addr> |
      | siteUtil | <util> |
      | siteTime | <tz> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${bSiteId}/location
    Then validate ${bAuth} is ${newId}
    Then receives status <code> <msg>

    Examples:
      | addr | util | tz | code | msg |
      | 1 Embarcadero Center, San Francisco, CA 94111 | 123456 | America/Los_Angeles | 400  | BAD_REQUEST |
      | 1 Embarcadero Center, San Francisco, CA 94111 | true | America/Los_Angeles | 400  | BAD_REQUEST |


    # POST /messages returned , 5ae4be72-a727-47e6-a75b-d2140a5b5d7e
    # GET /messages/5ae4be72-a727-47e6-a75b-d2140a5b5d7e
    # DELETE /messages/c83755d6-b7f5-40af-abf5-eae6d03227a3



  @supply
  Scenario: Verify supply rates for dev env
    Given http-client "rate_adapter"
    When sends GET /tariffs?tariffType=Supply&includeDuplicates=false
    #Electric Utility in Puerto Rico
    Then validate $[15].code is CNFL_Supply
    Then validate $[15].name is CNFL_TMT_Supply
    Then validate $[15].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[0].code is CNFL_Supply_usd
    Then validate $[0].name is CNFL_TMT_Supply US$
    Then validate $[0].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[13].code is TMTsupply
    Then validate $[13].name is ICE TMT Supply Rate
    Then validate $[13].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[6].code is TMTsupply_usd
    Then validate $[6].name is ICE TMT Supply Rate US$
    Then validate $[6].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[2].code is flatelabs
    Then validate $[2].name is Elabs Flat supply rate
    Then validate $[2].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[14].code is flatb15
    Then validate $[14].name is Elabs B15 Flat supply rate
    Then validate $[14].utilityId is 68913276-27be-4387-bb66-c690899c7546
    #ConEd NY
    Then validate $[12].code is NYISO_ZoneJ
    Then validate $[12].name is NY ISO Zone J
    Then validate $[12].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[9].code is flat08
    Then validate $[9].name is NY ConEd Flat $0.08 /kwh
    Then validate $[9].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[8].code is flat105
    Then validate $[8].name is NY ConEd Flat $0.105 /kwh
    Then validate $[8].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[1].code is NYISO_ZoneJ_MGV
    Then validate $[1].name is NY ISO Zone J - MGV
    Then validate $[1].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[4].code is flat08444
    Then validate $[4].name is NY ConEd Flat $0.08444 /kwh
    Then validate $[4].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    #Unknown
    Then validate $[3].code is HOEP_PREDICTED
    Then validate $[3].name is Predicted HOEP Supply
    Then validate $[3].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    #PG&E
    Then validate $[7].code is E19supply
    Then validate $[7].name is Cal PGE E19 Energy Supply Rate
    Then validate $[7].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[5].code is E20supply
    Then validate $[5].name is Cal PGE E20 Energy Supply Rate
    Then validate $[5].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    #NSW Utility AUSTRALIA
    Then validate $[10].code is varTouNSW
    Then validate $[10].name is Var TOU New South Wales
    #HydroOne CANADA
    Then validate $[11].code is HOEP
    Then validate $[11].name is HOEP Supply
    Then validate $[11].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    And receives status 200 OK

  @delivery
  Scenario: Verify delivery rates for dev env
    Given http-client "rate_adapter"
    When sends GET /tariffs?tariffType=Delivery&includeDuplicates=false
    #TEST UTILITY
    Then validate $[1].code is CSTE19
    Then validate $[1].name is TarBaby_123
    Then validate $[1].utilityId is 10f972fc-d541-44bf-b7c4-5e5e157c9994
    Then validate $[1].utilityName is Town of Ruston
    Then validate $[26].code is H1B
    Then validate $[26].name is HOGS
    Then validate $[26].utilityId is 10f972fc-d541-44bf-b7c4-5e5e157c9994
    Then validate $[26].utilityName is Town of Ruston
    #TEST UTILITY
    #SDGE
    Then validate $[0].code is SDGE_AL-TOU_Secondary-Inside
    Then validate $[0].name is AL-TOU Secondary Inside City Limits (UNVERIFIED)
    Then validate $[0].utilityId is 77e5bbb2-74cc-4f84-86af-51b0d56b43bf
    Then validate $[0].utilityName is San Diego Gas & Electric
    Then validate $[14].code is DGR_Secondary-Inside
    Then validate $[14].name is DG-R Secondary Inside City Limits – Solar-Only (UNVERIFIED)
    Then validate $[14].utilityId is 77e5bbb2-74cc-4f84-86af-51b0d56b43bf
    Then validate $[14].utilityName is San Diego Gas & Electric
    #SoCalEd
    Then validate $[2].code is SCE_Domestic
    Then validate $[2].name is Southern California Domestic
    Then validate $[2].utilityId is 3eb7bd33-9402-412f-8eb0-9eca02245c27
    Then validate $[2].utilityName is Southern California Edison Co
    Then validate $[10].code is SCE_TOU8R_Solar_below2kV
    Then validate $[10].name is SCE TOU-8 R <2kV Solar-Only (UNVERIFIED)
    Then validate $[10].utilityId is 3eb7bd33-9402-412f-8eb0-9eca02245c27
    Then validate $[10].utilityName is Southern California Edison Co
    Then validate $[15].code is SCE_TOU8B_below2kV
    Then validate $[15].name is SCE TOU-8 B < 2kV (UNVERIFIED)
    Then validate $[15].utilityId is 3eb7bd33-9402-412f-8eb0-9eca02245c27
    Then validate $[15].utilityName is Southern California Edison Co
    Then validate $[16].code is test
    Then validate $[16].name is Test Tariff
    Then validate $[16].utilityId is 3eb7bd33-9402-412f-8eb0-9eca02245c27
    Then validate $[16].utilityName is Southern California Edison Co
    Then validate $[24].code is SCE_TOU8B_below2kV_UNVERIFIED
    Then validate $[24].name is SCE TOU-8 B < 2kV
    Then validate $[24].utilityId is 3eb7bd33-9402-412f-8eb0-9eca02245c27
    Then validate $[24].utilityName is Southern California Edison Co
    #Hydro1CANADA
    Then validate $[3].code is H1ST_away_common
    Then validate $[3].name is Hydro One Sub Transmission - common (UNVERIFIED)
    Then validate $[3].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    Then validate $[3].utilityName is Hydro One Canada
    Then validate $[5].code is H1ST_away_high-high
    Then validate $[5].name is Hydro One Sub Transmission - high/high
    Then validate $[5].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    Then validate $[5].utilityName is Hydro One Canada
    Then validate $[12].code is H1ST_away_low-high
    Then validate $[12].name is Hydro One Sub Transmission - low/high (UNVERIFIED)
    Then validate $[12].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    Then validate $[12].utilityName is Hydro One Canada
    Then validate $[13].code is H1ST_away_low-low
    Then validate $[13].name is Hydro One Sub Transmission - low/low (UNVERIFIED)
    Then validate $[13].utilityId is 6a6f5607-434e-4d68-ae54-091a459fde19
    Then validate $[13].utilityName is Hydro One Canada
    #ConEdNY
    Then validate $[4].code is SC9RateIV
    Then validate $[4].name is NY ConEd SC9 RateIV
    Then validate $[4].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[4].utilityName is Consolidated Edison Co-NY Inc
    Then validate $[9].code is SC12RateII
    Then validate $[9].name is NY ConEd SC12 RateII
    Then validate $[9].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[9].utilityName is Consolidated Edison Co-NY Inc
    Then validate $[18].code is SC12RateV-RiderQ
    Then validate $[18].name is NY ConEd SC12 Rate V Rider Q
    Then validate $[18].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[18].utilityName is Consolidated Edison Co-NY Inc
    Then validate $[25].code is SC9RateI
    Then validate $[25].name is NY ConEd SC9 Rate I
    Then validate $[25].utilityId is abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $[25].utilityName is Consolidated Edison Co-NY Inc
    #PuertoRicanUtility
    Then validate $[6].code is CNFL_TMT_USD
    Then validate $[6].name is CNFL T-MT US$
    Then validate $[6].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[6].utilityName is Instituto Costarricense de Electricidad
    Then validate $[7].code is CNFL_TMT
    Then validate $[7].name is CNFL T-MT
    Then validate $[7].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[7].utilityName is Instituto Costarricense de Electricidad
    Then validate $[8].code is CNFL_TMT__USD_OE
    Then validate $[8].name is CNFL T-MT US$ OE
    Then validate $[8].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[8].utilityName is Instituto Costarricense de Electricidad
    Then validate $[11].code is TMT_usd_oe
    Then validate $[11].name is T-MT commercial rate US$ OE
    Then validate $[11].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[11].utilityName is Instituto Costarricense de Electricidad
    Then validate $[19].code is B15
    Then validate $[19].name is B-15 Rio Grande
    Then validate $[19].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[19].utilityName is Instituto Costarricense de Electricidad
    Then validate $[27].code is TMT
    Then validate $[27].name is T-MT commercial rate
    Then validate $[27].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[27].utilityName is Instituto Costarricense de Electricidad
    Then validate $[28].code is TMT_usd
    Then validate $[28].name is T-MT commercial rate US$
    Then validate $[28].utilityId is 68913276-27be-4387-bb66-c690899c7546
    Then validate $[28].utilityName is Instituto Costarricense de Electricidad
    #PG&E
    Then validate $[17].code is E-19-TOU-R_Secondary-Solar
    Then validate $[17].name is E-19 TOU-R Secondary Solar-Only (UNVERIFIED)
    Then validate $[17].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[17].utilityName is Pacific Gas & Electric Co
    Then validate $[20].code is E19_SGI_Ione
    Then validate $[20].name is PGE E19 SGI Ione
    Then validate $[20].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[20].utilityName is Pacific Gas & Electric Co
    Then validate $[21].code is E19
    Then validate $[21].name is PGE E19
    Then validate $[21].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[21].utilityName is Pacific Gas & Electric Co
    Then validate $[22].code is E19_Secondary_UNVERIFIED
    Then validate $[22].name is E19 (Secondary)
    Then validate $[22].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[22].utilityName is Pacific Gas & Electric Co
    Then validate $[23].code is E19_Secondary
    Then validate $[23].name is E19 Secondary (UNVERIFIED)
    Then validate $[23].utilityId is 257a47bd-849f-45d9-9e04-8d4d271839be
    Then validate $[23].utilityName is Pacific Gas & Electric Co
    And receives status 200 OK

  @regression
  Scenario Outline: Get <tariffType> Tariffs
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends GET /tariffs?tariffType=<tariffType>
    And receives status <code> <msg>

    Examples:
      | adapt | tariffType | code | msg |
      | rate  | Delivery   | 200  | OK  |
      | rate  | Supply     | 200  | OK  |
      | rate  | DayAhead   | 200  | OK  |

  @regression
  Scenario: Get rate from rate svc individual rate item
    Given http-client "rate_adapter"
    When sends GET /tariffs/supply/flat105
    Then validate $.timeOfUseRates.rate[0].rateDateRanges[0].rate is 0.105
    And receives status 200 OK