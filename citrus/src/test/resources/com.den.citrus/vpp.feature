Feature: Vpp Manager

  Background:
    Given variables
      | frDate   | 2018-01-01T01:01:01|
      | toDate   | 2019-12-31T23:56:04|
      | bool     | true               |
      | numz     | 12345678           |
      | badDate1 | 01/01/2019T01:01:01 |
      | badDate2 | 2019-01-01 |
      | nonUUIDv4 | 16caf036-b56d-4e0f-b57e-9153bc0ad1b1 |
      | goodUUIDv4 | 1a90284f-67d4-46fb-9f82-e48ab957a59f |
      | testUUIDv4 | 12345678-1234-4123-b123-9153bc0ad1b1 |
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |

  @regression @test
  Scenario Outline: <act> /maneuvers/.. cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | qString | <qString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${url}/${qString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | qString | note |
      | vpp | /manevers | 404 | NOT_FOUND | ${nonUUIDv4} | mispellingUrl |
      | vpp | /maneuvers | 404 | NOT_FOUND | ${nonUUIDv4} | non-existent-id |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${frDate} | date-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${bool} | bool-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${numz} | nums-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${ise500} | isoChars-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${badChr} | chars-asId |
      | vpp | /maneuvers | 200 | OK | ${nonUUIDv4}/participation | forgiveness-participation-non-existent-id |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${frDate}/participation | participation-date-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${bool}/participation | participation-bool-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${numz}/participation | participation-nums-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${ise500}/participation| participation-isoChars-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ${badChr}/participation | participation-chars-asId |

  @regression @test
  Scenario Outline: Vpp - <act> /maneuvers cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | qString | <qString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${url}${qString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | qString | note |
      | vpp | /manevers | 404 | NOT_FOUND | ?groupId=${nonUUIDv4} | mispellingUrl |
      | vpp | /maneuvers | 404 | NOT_FOUND | ?groupId=${nonUUIDv4} | non-existent-id |
      | vpp | /maneuvers | 404 | NOT_FOUND | &groupId=${nonUUIDv4} | badChar-for-Qstring-id |
      | vpp | /maneuvers | 404 | NOT_FOUND | groupId=${nonUUIDv4} | missingChar-for-Qstring-id |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ?groupId=${frDate} | date-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ?groupId=${bool} | bool-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ?groupId=${numz} | nums-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ?groupId=${ise500} | isoChars-asId |
      | vpp | /maneuvers | 400 | BAD_REQUEST | ?groupId=${badChr} | chars-asId |
