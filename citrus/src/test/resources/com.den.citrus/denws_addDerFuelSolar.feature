Feature: DER (within DEN.OS Cloud >> Demand Energy >> QE)
## this feature REQUIRES that you parse the POST location's response to get the new location's ID, then add accordingly to rest of tests.

  Background:
    Given variables
      | uSite   | /sites  |
      | uStore  | /stores |
      | rstUrl  | /rest   |
      | uAcct   | /accounts |
      | qeAcct  | 768b8574-d513-4b48-a5cc-1401808078ea |
      | bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |

  @working
  Scenario: Test health endpoint for service running on port specified in citrus-application.properties
    Given http-client "httpClient"
    When sends GET /health
    Then validate $.status is UP
    And receives status 200 OK

  @working
  Scenario: Adding a DER location (withing DEN.OS Cloud >> Demand Energy >> QE)
    Given http-client "denws_adapter"
    Given variables
      | newLocName | TestSite1357 |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends POST ${rstUrl}${uAcct}/${qeAcct}${uSite}/${newLocName}
    Then receives status 200 OK

  @working
  Scenario: Adding DER battery store with 2 racks to a location
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | 739ea085-c357-4524-b1e5-88451d3b0b48 |
      | battery_store_name | test_123_battery_site  |
      | store_type         | DEN-STORE                 |
      | battery_rack1_name | test_123_rack1            |
      | battery_rack2_name | test_123_rack2            |
      | batterySystem      | Enersys                   |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":105,
        "racks":[
          {"id":null,"name":"${battery_rack1_name}","rackNumber":1},
          {"id":null,"name":"${battery_rack2_name}","rackNumber":2}
        ],
        "maxDOD":65,
  "batterySystem":${batterySystem}
  }
  """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK

  @working
  Scenario: Adding DER solar cell to a location
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | 739ea085-c357-4524-b1e5-88451d3b0b48 |
      | battery_store_name | test_123_solar_site  |
      | store_type         | SOLAR                   |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":null,
        "racks":[],
      }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK

  @working
  Scenario: Adding DER fuel cell to a location
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | 739ea085-c357-4524-b1e5-88451d3b0b48 |
      | battery_store_name | test_123_fuelcell      |
      | store_type         | FUEL-CELL                 |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":null,
        "racks":[],
      }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK
