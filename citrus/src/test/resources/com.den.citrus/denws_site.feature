Feature:  denws - /rest/... & etc related tests

  Background:
    Given variables
      | uRest   | /rest |
      | uSite   | /sites  |
      | uStore  | /stores |
      | uRate   | /rates  |
      | rstUrl  | /rest   |
      | uAcct   | /accounts |
      | vUrl    | /version  |
      | qName   | James     |
      | qeBatSy | Enersys   |
      | qeStrTp | DEN-STORE |
      | dvVers  | develop-2127 |
      | qeMaxDOD  | 65         |
      | qeSiteNm  | TestSite1  |
      | qeStorNm  | Battery1   |
      | qeRak1Nm  | ESS Rack 1 |
      | qeRak2Nm  | ESS Rack 2 |
      | qeAvailPower  | 105    |
      | qeNumBattery  | 40     |
      | qeNamBatEner  | 203.52 |
      | newLocName    | qe_automate_site_1 |
      | qeStore | 768b8574-d513-4b48-a5cc-1401808078ea  |
      | qeDvcId | 672cb9a6-b742-4c95-8388-648c9358f771  |
      | qeStrId | 88b2c7a1-435c-45b2-8a74-20653aac221a  |
      | qeRak1  | a6d41e60-680e-4dcd-8ce0-d04d69d7add4  |
      | qeRak2  | 952e8e00-7b34-44ca-ba24-fd0b09fe3a24  |
      | qeStoreSiteId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
#      | dev_bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
      | dev_bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | soak_bAuth| Basic UUFhdXRvbWF0aW9uOmY0YzBjMmU3LWI0NzgtNDAzMi05YTVhLWUxMjUyZDJhMWI5Nw== |

  @regression @dev
  Scenario: Validate denws /version API
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 200 OK

  @regression @dev
  Scenario: V: Auth attempt w/ SOAK creds fails
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 401 UNAUTHORIZED

  #site_specific
  Scenario: V: sales-modeling submission state - finished submission
    Given http-client "denws_adapter"
    Given variables
      | siteId |  be7f4250-5165-484c-9596-26fbc9782fc7 |
      | fileName | Boston_601_Take2_5min_Jan16.csv     |
      | salesSt  | finished                            |
      | count    | 8928                                |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
    Then validate $[0].fileName is ${fileName}
    Then validate $[0].status is ${salesSt}
    Then validate $[0].count is ${count}
    Then receives status 200 OK

  #site_specific
  Scenario: V: sales-modeling submission state - finished submission
    Given http-client "denws_adapter"
    Given variables
      | siteId |  1048362d-34f4-4db9-8c3d-7fc6e40bcc81 |
      | fileName | Fletcher Jones Sales Modeling Demo.csv     |
      | salesSt  | finished                            |
      | count    | 8835                                |
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends GET ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
    Then validate $[0].fileName is ${fileName}
    Then validate $[0].status is ${salesSt}
    Then validate $[0].count is ${count}
    Then receives status 200 OK

  #the following tests work with already posted accounts w/ test data loaded
  #validating first few entries from fixes sources
  #site_specific
  Scenario Outline: denws /demand info - <interv> min intervals - site : <siteId> - specific time range
    Given http-client "<adapt>_adapter"
    Given variables
      | intervala | demand30min                       |
      | intervalb | adjustedDemand30min               |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${rstUrl}${uSite}/<siteId>/statistics/demand?startDate=<start>&stopDate=<end>
    Then validate $.${intervala}.[0][1] is <intA>
    Then validate $.${intervala}.[1][1] is <intB>
    Then validate $.${intervala}.[2][1] is <intC>
    Then validate $.${intervala}.[3][1] is <intD>
    Then validate $.${intervalb}.[0][1] is <intE>
    Then receives status <code> <msg>

    Examples:
      | adapt | siteId | start | end | code | msg | intA | intB | intC | intD | intE | interv |
      | denws | f3c96c6a-dd50-40ee-9aa8-65b9fe6d640b | 2018-01-03T00:00:00-08:00 | 2018-01-08T23:59:59-08:00 | 200 | OK | 244.8 | 243.36 | 241.92 | 239.04 | 249.254220165909 | 30 |
      | denws | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | 2018-06-03T00:00:00-08:00 | 2018-06-08T23:59:59-08:00 | 200 | OK | 243.36 | 240.48 | 239.04 | 236.16 | 243.36 | 30 |

  #validating entries from 30 min window on a fixed source
  #site_specific
  Scenario Outline: denws /combinedDemand info - <interv> min intervals - site : <siteId> - specific time range
    Given http-client "<adapt>_adapter"
    Given variables
      | intervala | demand                       |
      | intervalb | adjustedDemand               |
      | intervalc | partiallyAdjusted            |
      | descA | BUILDING_PLUS_SOLAR              |
      | descB | BUILDING_PLUS_SOLAR_STORAGE      |
      | descC | BUILDING_PLUS_STORAGE            |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${rstUrl}${uSite}/<siteId>/statistics/combinedDemand?startDate=<start>&stopDate=<end>
    Then validate $.${intervalb}.[0][1] is <intA>
    Then validate $.${intervala}.[0][1] is <intB>
    Then validate $.${intervalc}.[0].descriptor is ${descA}
    Then validate $.${intervalc}.[0].stats.[0][1] is <intC>
    Then validate $.${intervalc}.[1].descriptor is ${descB}
    Then validate $.${intervalc}.[1].stats.[0][1] is <intD>
    Then validate $.${intervalc}.[2].descriptor is ${descC}
    Then validate $.${intervalc}.[2].stats.[0][1] is <intE>
    Then receives status <code> <msg>

    Examples:
      | adapt | siteId | start | end | code | msg | intA | intB | intC | intD | intE | interv |
      | denws | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | 2018-06-03T00:00:00-08:00 | 2018-06-03T00:00:00-08:00 | 200 | OK | 243.36 | 243.36 | 243.36 | 243.36 | 243.36 | 30 |
      | denws | f3c96c6a-dd50-40ee-9aa8-65b9fe6d640b | 2018-01-03T00:00:00-08:00 | 2018-01-08T23:59:59-08:00 | 200 | OK | 249.254220165909 | 244.8 | 244.8 | 249.254220165909 | 249.254220165909 | 30 |

  #validating entries from 30 min window on a fixed source
  #site_specific
  Scenario Outline: denws /<type>UnitCharges - <interv> min intervals - start: <start> , end: <end>
    Given http-client "<adapt>_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${rstUrl}${uSite}/<siteId>/statistics/cost/<type>UnitCharges?startDate=<start>&stopDate=<end>
    Then validate $.[0][1] is <intA>
    Then validate $.[1][1] is <intA>
    Then validate $.[2][1] is <intA>
    Then validate $.[3][1] is <intA>
    Then validate $.[4][1] is <intA>
    Then validate $.[5][1] is <intA>
    Then receives status <code> <msg>

    Examples:
      | adapt | siteId | type | start | end | code | msg | intA | interv |
      | denws | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | demand | 2018-06-03T00:00:00-08:00 | 2018-06-03T00:29:00-08:00 | 200 | OK | 21.34 | 30 |
      | denws | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | energy | 2018-06-03T00:00:00-08:00 | 2018-06-03T00:29:00-08:00 | 200 | OK | 0.095| 30 |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | demand | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 0.682 | 30 |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | energy | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 0.358 | 30 |

  #validating entries from 30 min window on a fixed source
  #site_specific
  Scenario Outline: denws <type>-<interv> charge rates & names reconcile, speciic acct, in <range> min intervals - start: <start> , end: <end>
    Given http-client "<adapt>_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${rstUrl}${uSite}/<siteId>/billing/results/<interv>/?startDate=<start>&stopDate=<end>
    Then validate $[0].additionalInfo is <rate>
    Then validate $[0].key is <key>
    Then validate $[0].value is 598.2864516090001
    Then validate $[0].items.[0].key is <sr1>
    Then validate $[0].items.[1].key is <sr2>
    Then validate $[0].items.[2].key is <sr3>
    Then receives status <code> <msg>

    Examples:
      | adapt | siteId | type | interv | key | start | end | code | msg | range | rate | sr1 | sr2 | sr3 |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | billing | before | Supply | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 30 | NY ConEd Flat $0.105 /kwh | Energy Supply | Energy Supply Taxes-GRT | Energy Supply Taxes-Sales Tax |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | billing | after | Supply | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 30 | NY ConEd Flat $0.105 /kwh | Energy Supply | Energy Supply Taxes-GRT | Energy Supply Taxes-Sales Tax |

  #site_specific
  Scenario Outline: denws <type>-<interv> charge kWh & percentage reconcile, speciic acct, in <range> min intervals - start: <start> , end: <end>
    Given http-client "<adapt>_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${rstUrl}${uSite}/<siteId>/billing/results/<interv>/?startDate=<start>&stopDate=<end>
    Then validate $[0].additionalInfo is <rate>
    Then validate $[0].key is <key>
    Then validate $[0].value is 598.2864516090001
    Then validate $[0].items.[0].additionalInfo is <sr1>
    Then validate $[0].items.[0].value is <srA>
    Then validate $[0].items.[1].additionalInfo is <sr2>
    Then validate $[0].items.[1].value is <srB>
    Then validate $[0].items.[2].additionalInfo is <sr3>
    Then validate $[0].items.[2].value is <srC>
    Then receives status <code> <msg>

    Examples:
      | adapt | siteId | type | interv | key | start | end | code | msg | range | rate | sr1 | sr2 | sr3 | srA | srB | srC |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | billing | before | Supply | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 30 | NY ConEd Flat $0.105 /kwh | 5,190.48kWh | 5.05% | 4.5% | 545.0004 | 27.522520200000002 | 25.763531409000002 |
      | denws | 7ec8759f-33e4-464a-bc34-a73198d84f47 | billing | after | Supply | 2018-01-04T13:00:00-08:00 | 2018-01-04T13:29:00-08:00 | 200 | OK | 30 | NY ConEd Flat $0.105 /kwh | 5,190.48kWh | 5.05% | 4.5% | 545.0004 | 27.522520200000002 | 25.763531409000002 |
