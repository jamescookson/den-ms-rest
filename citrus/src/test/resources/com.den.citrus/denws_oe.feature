Feature: oe-svc endpoints

  Background:
    Given variables
      | sStatus  | UP       |
      | statUp   | UP       |
      | statDn   | DOWN     |
      | cfgUrl   | /config  |
      | gString  | group_id |

  @regression @soak
  Scenario: Validate oe-service /config API endpoint
    Given http-client "oe_adapter"
    When sends GET ${cfgUrl}
    Then receives status 200 OK

  Scenario: Validate specific entries from a known posted config
    Given http-client "oe_adapter"
    Given variables
      | groupID     | 23fa79af-eab0-412f-b4a4-3a7ad02918cf |
      | b_battName  | LG Chem 14S2 R600                    |
      | grpRate     | SDGE_AL-TOU_Secondary-Inside         |
      | supRate     | null                                 |
      | timeZone    | America/Los_Angeles                  |
      | l_ldType    | consumptive-load                     |
      | l_mtType    | building                             |
      | c_llv       | lower_limit_violation                |
      | c_bec       | battery_energy_change                |
      | startTime   | 2018-06-03T00:00-07:00               |
      | endTime     | 2018-06-08T23:59:59-07:00            |
    When sends GET ${cfgUrl}?${gString}=${groupID}
    Then validate $[0].siteId is ${groupID}
    Then validate $[0].deliveryRate is ${grpRate}
    Then validate $[0].timeZone is ${timeZone}
    Then validate $[0].battery.name is ${b_battName}
    Then validate $[0].capacity-loss-adjustment is 0.15
    Then validate $[0].loads[0].loadType is ${l_ldType)
    Then validate $[0].loads[0].meterType is ${l_mtType)
    Then validate $[0].costs[0] is ${c_llv)
    Then validate $[0].costs[2] is ${c_bec}
    Then validate $[0].date-range.start is ${startTime)
    Then validate $[0].date-range.end is ${endTime)
    Then receives status 200 OK
