Feature: Derp Service

  Background:
    Given variables
      | frDate   | 2018-01-01T01:01:01|
      | toDate   | 2019-12-31T23:56:04|
      | bool     | true               |
      | numz     | 12345678           |
      | badDate1 | 01/01/2019T01:01:01 |
      | badDate2 | 2019-01-01 |
      | goodUUIDv4 | 16caf036-b56d-4e0f-b57e-9153bc0ad1b1|
      | nonUUIDv4 | 12345678-1234-4123-b123-9153bc0ad1b1 |
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |

  @regression
  Scenario Outline: Get forecasts/ID - POS|NEG cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | qString | <qString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${url}/${qString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | qString | note |
      | derp  | /forecasts | 200 | OK | ${nonUUIDv4} | forgiveness-any-UUIDv4-like-entry |
      | derp  | /forecasts | 200 | OK | ${goodUUIDv4}?nextPage=${bool} | forgiveness-nextPage=accepts-anything-bool |
      | derp  | /forecasts | 200 | OK | ${goodUUIDv4}?nextPage=${numz} | forgiveness-nextPage=accepts-anything-number |
      | derp  | /forecasts | 200 | OK | ${goodUUIDv4}?nextPage=${nonUUIDv4} | forgiveness-nextPage=accepts-anything-date |
      | derp  | /forecasts | 200 | OK | ${goodUUIDv4}?nextPage=${badChr} | forgiveness-nextPage=accepts-anything-date |
      | derp  | /forecasts | 200 | OK | ${goodUUIDv4}?start=${frDate}&end=${toDate}&nextPage=${badChr} | bad-entry-forgiveness-nextPage |
      | derp  | /forccasts | 404 | NOT_FOUND | ${goodUUIDv4} | bad-forcasts-spelling |
      | derp  | /forecasts | 400 | BAD_REQUEST | ${bool} | boolean-entry-for-deviceID |
      | derp  | /forecasts | 400 | BAD_REQUEST | ${numz} | nonUUIDv4-number-entry-for-deviceID |
      | derp  | /forecasts | 400 | BAD_REQUEST | ${ise500} | badEntry-for-deviceID |
      | derp  | /forecasts | 404 | NOT_FOUND |   | nonEntry-for-deviceID |
      | derp  | /%20 | 404 | NOT_FOUND | ${goodUUIDv4} | nbsp-for-forcasts |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${badDate2} | bad-start-date-incomplete |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${badDate2} | bad-end-date-incomplete |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${badDate1} | bad-start-dateFormat |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${badDate1} | bad-end-dateFormat |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${bool} | bad-start-boolean |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${bool} | bad-end-boolean |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${numz} | bad-start-nums |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${numz} | bad-end-nums |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${badChr} | bad-start-badChr |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${badChr} | bad-end-badChr |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${ise500} | bad-start-iso500Chr |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?end=${ise500} | bad-end-iso500Chr |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${bool}&end=${frDate} | bad-qStringEntry-start |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${frDate}&end=${numz} | bad-qStringEntry-end |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${frDate}&end=${bool}&nextPage=${goodUUIDv4} | fullQueryString-one-bad-entry-endDate |
      | derp  | /forecasts | 500 | INTERNAL_SERVER_ERROR | ${goodUUIDv4}?start=${numz}&end=${toDate}&nextPage=${goodUUIDv4} | fullQueryString-one-bad-entry-startDate |
      | derp  | /forecasts | 400 | BAD_REQUEST | ${ise500}?start=${numz}&end=${toDate}&nextPage=${goodUUIDv4} | fullQueryString-one-bad-entry-devceID |
      | derp  | /forecasts | 400 | BAD_REQUEST | ${badChr}?start=${numz}&end=${toDate}&nextPage=${goodUUIDv4} | fullQueryString-bad-encodedChr-entry-devceID |
