Feature: Generic Sales Modeling-related.
  Background:
    Given variables
      | dev_bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
      | devStore | 768b8574-d513-4b48-a5cc-1401808078ea |
      | soak_bAuth| Basic UUFhdXRvbWF0aW9uOmY0YzBjMmU3LWI0NzgtNDAzMi05YTVhLWUxMjUyZDJhMWI5Nw== |
      | soakStore | e4fec7d5-f6fa-46a3-b5c1-2596092056f7 |
      | bSiteId | 25644172-4ddf-48d6-ac73-dd154266febe |
      | uStore  | /stores |
      | uRate   | /rates  |
      | rstUrl  | /rest   |
      | uAcct   | /accounts |
      | uSite   | /sites  |
      | metUrl  | /meta   |
      | cfgUrl  | /config |
      | tarUrl  | /tariffs  |
      | sUrl    | /schedules|
      | uAcct   | /accounts |
      | vUrl    | /version  |
      | utlUrl  | /utilities|
      | dayUrl  | /dayahead |
      | newLocName | RESTSite1 |

  Scenario: adding a site/location
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends POST ${rstUrl}${uAcct}/${soakStore}${uSite}/${newLocName}
    Then receives status 200 OK

  Scenario: Updating site with adressing, adding ConEd utility
    Given http-client "denws_adapter"
    Given variables
      | siteName | ${newLocName} |
      | siteAddress | 1133 Westchester Ave, New York City, NY 10604 |
      | siteUtil | ConEd |
      | siteTime | America/New_York |
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${bSiteId}/location
    Then receives status 200 OK

  Scenario: Adding a rate structure for ConEd
    Given http-client "denws_adapter"
    Given variables
      | delBefor | SC9RateIV |
      | delAfter | SC9RateIV |
      | supBefor | flat105 |
      | supAfter | flat105 |
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${delAfter},
      "deliveryBefore":${delBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":${supAfter}
    }
    """
    When sends PUT ${rstUrl}/sites/${bSiteId}/rates
    Then receives status 200 OK

  @notworking
  Scenario: Updating site with adressing, adding PGE utility
    Given http-client "denws_adapter"
    Given variables
      | siteAddress | 1 Embarcadero Center, San Francisco, CA 94111 |
      | siteUtil | PGE |
      | siteTime | America/Los_Angeles |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "SID":"${siteSID}",
      "deviceId":"${siteDeviceId}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${siteId}/location
    Then receives status 200 OK

  @notworking
  Scenario: Adding a rate structure for PGE
    Given http-client "denws_adapter"
    Given variables
      | delBefor | E19_Secondary |
      | delAfter | E19_Secondary |
      | supBefor | E20 |
      | supAfter | E20 |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${delAfter},
      "deliveryBefore":${delBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":{supAfter}
    }
    """
    When sends PUT /rest/sites/${bSiteId}/rates
    Then receives status 200 OK

  @notworking
  Scenario: Updating site DR Grouping
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | <add_from_post> |
      | siteName | <get_from_naming> |
      | siteSID | <add_from_post> |
      | siteDeviceId | <add_from_post> |
      | siteAddress | 1 Embarcadero Center, San Francisco, CA 94111 |
      | siteUtil | PGE |
      | siteTime | America/Los_Angeles |
      | siteTags | DRAM DR (Sales Beta) |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "SID":"${siteSID}",
      "deviceId":"${siteDeviceId}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${siteId}
    Then receives status 200 OK

  @notworking
  Scenario: Adding DER to site w/rack of 10 for SM sim.
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | <add_from_post> |
      | siteName | <get_from_naming> |
      | siteSID | <add_from_post> |
      | siteDeviceId | <add_from_post> |
      | siteAddress | 1 Embarcadero Center, San Francisco, CA 94111 |
      | siteUtil | PGE |
      | siteTime | America/Los_Angeles |
      | siteTags | DRAM DR (Sales Beta) |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":null,
      "name":${bSiteName},
      "SID":null,
      "type":${siteStore},
      "availablePower":995,
      "racks":[
        {"id":null,"name":"ESS Rack 1","rackNumber":1},
        {"id":null,"name":"ESS Rack 2","rackNumber":2},
        {"id":null,"name":"ESS Rack 3","rackNumber":3},
        {"id":null,"name":"ESS Rack 4","rackNumber":4},
        {"id":null,"name":"ESS Rack 5","rackNumber":5},
        {"id":null,"name":"ESS Rack 6","rackNumber":6},
        {"id":null,"name":"ESS Rack 7","rackNumber":7},
        {"id":null,"name":"ESS Rack 8","rackNumber":8},
        {"id":null,"name":"ESS Rack 9","rackNumber":9},
        {"id":null,"name":"ESS Rack 10","rackNumber":10}
      ],
      "maxDOD":95,
      "batterySystem":"LG Chem 14S2 R600"
    }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK

  @notworking
  Scenario: Adding solar cell to a location
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | <add_from_post> |
      | battery_store_name | test_123_solar_site  |
      | store_type         | SOLAR                |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":null,
        "racks":[],
      }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK
