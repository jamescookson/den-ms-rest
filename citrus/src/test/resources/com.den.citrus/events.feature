Feature: Events Service

  Background:
    Given variables
      | frDate   | 2018-01-01T01:01:01|
      | toDate   | 2019-12-31T23:56:04|
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |

  @regression
  Scenario Outline: <descr>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends GET <url>
    And receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | descr |
      | eventsvc    | /health  | 200 | OK  | health |
      | eventsvc    | /info  | 200 | OK  | info |

  @regression
  Scenario Outline: Add event, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | dateTime | <dt> |
      | deviceId | <dev> |
      | devName | <devName> |
      | name | <name> |
      | sev | <sev> |
      | networkId | <netId> |
      | value | <freetext> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "dateTime":"${dateTime}",
      "deviceId":"${deviceId}",
      "deviceName":"${devName}",
      "name":${name},
      "severity":"${sev}",
      "networkId":"${networkId}",
      "value":"${value}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | dt | dev | devName | name | sev | netId | freetext | note |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | 123456 | devSiteGru | devSiteGru | info | 123456 | freecel | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | textcell | devSiteGru | devSiteGru | info | 123456 | freecel | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | true | devSiteGru | devSiteGru | info | 123456 | freecel | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | 123456 | freecel | bad network-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | textcell | freecel | bad network-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 0000-01-01T00:00:01 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | true | freecel | bad network-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 03/04/2019 16:00:00 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | freecel | bad date/time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | 14:00:00T2019-03-01 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | textcell | freecel | bad date/time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | March 18, 2019 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | true | freecel | bad date/time |
      | eventsvc    | /event  | 404 | NOT_FOUND  | 0000-01-01T00:00:01 | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 | devSiteGru | devSiteGru | info | true | freecel | bad date/time |

  @regression
  Scenario Outline: Add svc-event, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | utc | <dt> |
      | src | <src> |
      | pay | <payload> |
      | name | <name> |
      | sev | <sev> |
      | networkId | <netId> |
      | value | <val> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "source":"${src}",
      "name":"${name}",
      "payload":"${pay}",
      "utcTimestamp":${utc},
      "severity":"${sev}",
      "networkId":"${networkId}",
      "value":"${value}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | dt | src | name | payload | sev | netId | val | note |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 2019-01-01T00:00:01 | 123456 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad device-id |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 2019-01-01T00:00:01 | hello | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad device-id |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 2019-01-01T00:00:01 | true | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad device-id |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 2019-01-01T00:00:01 | 2019-01-01T00:00:01 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad device-id |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 2019-01-01T00:00:01 |  | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | no device-id |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | Jan 1, 2019 01:01:01 | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 00:00:01 2019-04-01 | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 4/14/2018 05:00:00 | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | 77207691-f779-458b-867e-86c9cdf88d67 | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | hello | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  | true | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | bad date/time |
      | eventsvc    | /events/service-event  | 400 | BAD_REQUEST  |  | 77207691-f779-458b-867e-86c9cdf88d67 | devSiteGru | freecel | info | 77207691-f779-458b-867e-86c9cdf88d67 | test1234 | no date/time |
    # add cases for sev,netId,value

  @regression
  Scenario Outline: Get events, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | qString | <qString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${url}${qString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | qString | note |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=troll&start-date=${frDate}&stop-date=${toDate} | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=&start-date=${frDate}&stop-date=${toDate} | no-device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&network-id=${badTxt}&start-date=${frDate}&stop-date=${toDate} | device-id-with-bad-netId-badText |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&network-id=${frDate}&start-date=${frDate}&stop-date=${toDate} | device-id-with-bad-netId-dateString |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&network-id=true&start-date=${frDate}&stop-date=${toDate} | device-id-with-bad-netId-val=true |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=test1234&start-date=${frDate}&stop-date=${toDate} | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=true&start-date=${frDate}&stop-date=${toDate} | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=%20&start-date=${frDate}&stop-date=${toDate} | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=&start-date=${frDate}&stop-date=${toDate} | bad device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=4/14/2019T14:14:14&stop-date=${toDate} | bad-start-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=%20&stop-date=${toDate} | bad-start-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=true&stop-date=${toDate} | bad-start-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=${frDate}&stop-date=4/14/2019T14:14:14 | bad-end-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=${frDate}&stop-date=%20  | bad-end-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=${frDate}&stop-date=true | bad-end-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?start-date=2018-01-01t01:01:01&stop-date=2019-12-31T23:59:59 | missing-device-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&stop-date=2019-12-31T23:59:59 | missing-start-time |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=2018-01-01T23:59:59 | missing-end-time |
      | eventsvc    | /events  | 200 | OK  | ?device-id=7720af91-f779-458b-a67e-86c9c3e88d67&start-date=2018-01-01t01:01:01&stop-date=2019-12-31T23:59:59 | empty-body-non-existent-id |

  @regression
  Scenario Outline: Delete events, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | idString | <idString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends DELETE ${url}/${idString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | idString | note |
      | eventsvc    | /events  | 404 | NOT_FOUND  | 12345678-1234-4123-a123-123456781012 | no-id |
      | eventsvc    | /events  | 404 | NOT_FOUND  | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | previously-deleted-id |
      | eventsvc    | /events  | 404 | NOT_FOUND  | %20%20%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | chars-in-query |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${frDate} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${badTxt} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${badChr} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${ise500} | 400-bad-id |

  @regression
  Scenario Outline: Get in event ID, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | idString | <idString> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${url}/${idString}
    Then receives status <code> <msg>
    Examples:
      | adapt | url | code | msg | idString | note |
      | eventsvc    | /events  | 404 | NOT_FOUND  | 12345678-1234-4123-a123-123456781012 | no-id |
      | eventsvc    | /events  | 404 | NOT_FOUND  | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | previously-deleted-id |
      | eventsvc    | /events  | 404 | NOT_FOUND  | %20%20%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | chars-in-query |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${frDate} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${badTxt} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${badChr} | 400-bad-id |
      | eventsvc    | /events  | 400 | BAD_REQUEST  | ${ise500} | 400-bad-id |
