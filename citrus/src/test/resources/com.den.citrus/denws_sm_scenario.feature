Feature: Sales Modeling Scenario-related : DER+Solar+Upload+SM

  @SM_Scenario1 @ConEd
  Scenario: Updating site with adressing, adding ConEd utility
    Given http-client "denws_adapter"
    Given variables
      | bSiteId | 67565832-6857-4fa1-abe2-a4e7c93ea00e |
      | siteName | dkjfdkjfdkj |
      | siteSID | 1b3763d9-d6f0-488b-afc1-9a5c23af972e |
      | siteDeviceId | b11c4017-8b19-4513-8ce8-1c7efe60f8c6 |
      | siteAddress | 1133 Westchester Ave, New York City, NY 10604 |
      | siteUtil | ConEd |
      | siteTime | America/New_York |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "SID":"${siteSID}",
      "deviceId":"${siteDeviceId}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${siteId}/location
    Then receives status 200 OK

  @SM_Scenario1 @ConEd
  Scenario: Adding a rate structure for ConEd
    Given http-client "denws_adapter"
    Given variables
      | bSiteId  | be7f4250-5165-484c-9596-26fbc9782fc7 |
      | delBefor | SC9RateIV |
      | delAfter | SC9RateIV |
      | supBefor | flat105 |
      | supAfter | flat105 |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${delAfter},
      "deliveryBefore":${delBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":{supAfter}
    }
    """
    When sends PUT /rest/sites/${bSiteId}/rates
    Then receives status 200 OK


    #Adding Rack of 10 for SM sim.
    @SM_Scenario2 @ConEd @DER
    Scenario: Adding DER to site w/rack of 2 for SM sim.
      Given http-client "denws_adapter"
      Given variables
        | bSiteId | <add_from_post> |
        | battery_store_name | test_123_battery_site  |
        | store_type         | DEN-STORE                 |
        | battery_rack1_name | test_123_rack1            |
        | battery_rack2_name | test_123_rack2            |
        | batterySystem      | Enersys                   |
      And Content-Type: application/json
      And Header Authorization: ${bAuth}
      And Payload:
      """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":105,
        "racks":[
          {"id":null,"name":"${battery_rack1_name}","rackNumber":1},
          {"id":null,"name":"${battery_rack2_name}","rackNumber":2}
        ],
        "maxDOD":65,
        "batterySystem":${batterySystem}
      }
      """
      When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
      Then receives status 200 OK

    @SM_Scenario2 @ConEd @SOLAR
    Scenario: Adding solar cell to a location
      Given http-client "denws_adapter"
      Given variables
        | bSiteId | <add_from_post> |
        | battery_store_name | test_123_solar_site  |
        | store_type         | SOLAR                |
      And Content-Type: application/json
      And Header Authorization: ${bAuth}
      And Payload:
      """
        {
          "id":null,
          "SID":null,
          "name":${battery_store_name},
          "type":${store_type},
          "availablePower":null,
          "racks":[],
        }
      """
      When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
      Then receives status 200 OK

##### scenario2_file #####
#Feature: Sales Modeling Scenario-related : DER+Solar+DRAMGroup+Upload+SM

#  Background:
#    Given variables
#      | uRest   | /rest |
#      | uSite   | /sites  |
#      | uStore  | /stores |
#      | uRate   | /rates  |
#      | rstUrl  | /rest   |
#      | metUrl  | /meta   |
#      | cfgUrl  | /config |
#      | tarUrl  | /tariffs  |
#      | sUrl    | /schedules|
#      | uAcct   | /accounts |
#      | vUrl    | /version  |
#      | utlUrl  | /utilities|
#      | dayUrl  | /dayahead |
#      | siteName | SMDER_Site123 |
#      | qeStore | 768b8574-d513-4b48-a5cc-1401808078ea |
#      | bSiteId | 5c0259de-b593-41dd-a294-f21ca0846858 |
#      | siteSID | 3487c38f-074a-45a1-936e-5b488308f64b |
#      | siteDeviceId | a3075a07-6aad-4d4b-9597-7fa487739586 |
#      | dev_bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
#      | soak_bAuth| Basic UUFhdXRvbWF0aW9uOmY0YzBjMmU3LWI0NzgtNDAzMi05YTVhLWUxMjUyZDJhMWI5Nw== |

#  Scenario name --> (state)
#  Scenario: Adding a DER location (withing DEN.OS Cloud >> Demand Energy >> QE) --> works
#  Scenario: Updating site with adressing, adding PGE utility --> testing now
#  Scenario: Adding a rate structure for PGE --> testing now
#  Scenario: Updating site DR Grouping --> testing now
#  Scenario: Adding DER to site w/rack of 10 for SM sim. --> testing now
#  Scenario: Adding solar cell to a location --> testing now
#  Scenario: uploading data file --> in dev|testing
#  Scenario: kicking off a SM --> in dev

#  @test
#  Given http-client "denws_adapter"
#  Given variables
#    | newLocName | SMDER_Site123 |
#  And Content-Type: application/json
#  And Header Authorization: ${dev_dev_bAuth}
#  When sends POST ${rstUrl}${uAcct}/${qeStore}${uSite}/${newLocName}
#  Then receives status 200 OK

  @SM_Scenario2 @PGE
  Scenario: Add address, add PGE utility
    Given http-client "denws_adapter"
    Given variables
      | siteAddress | 1 Embarcadero Center, San Francisco, CA 94111 |
      | siteUtil | PGE |
      | siteTime | America/Los_Angeles |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    And Payload:
    """
    {
      "id":"${bSiteId}",
      "name":"${siteName}",
      "SID":"${siteSID}",
      "deviceId":"${siteDeviceId}",
      "timezone":"${siteTime}",
      "utility":${siteUtil},
      "address":"${siteAddress}",
      "vppSiteConfigurationFactor":0.8,
      "tags":null
    }
    """
    When sends PUT rest/sites/${bSiteId}/location
    Then receives status 200 OK

  @SM_Scenario2 @PGE
  Scenario: Adding a rate structure for PGE
    Given http-client "denws_adapter"
    Given variables
      | delBefor | E19_Secondary |
      | delAfter | E19_Secondary |
      | supBefor | E20 |
      | supAfter | E20 |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${delAfter},
      "deliveryBefore":${delBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":{supAfter}
    }
    """
    When sends PUT /rest/sites/${bSiteId}/rates
    Then receives status 200 OK

#  @SM_Scenario2 @PGE @DR
#  Scenario: Updating site DR Grouping
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 5c0259de-b593-41dd-a294-f21ca0846858 |
#      | siteName | SMDER_Site123 |
#      | siteSID | 3487c38f-074a-45a1-936e-5b488308f64b |
#      | siteDeviceId | a3075a07-6aad-4d4b-9597-7fa487739586 |
#      | siteAddress | 1 Embarcadero Center, San Francisco, CA 94111 |
#      | siteUtil | PGE |
#      | siteTime | America/Los_Angeles |
#      | siteTags | DRAM DR (Sales Beta) |
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#    {
#      "id":"${bSiteId}",
#      "name":"${siteName}",
#      "SID":"${siteSID}",
#      "deviceId":"${siteDeviceId}",
#      "timezone":"${siteTime}",
#      "utility":${siteUtil},
#      "address":"${siteAddress}",
#      "vppSiteConfigurationFactor":0.8,
#      "tags":null
#    }
#    """
#    When sends PUT rest/sites/${siteId}
#    Then receives status 200 OK

  #Adding Rack of 10 for SM sim.
  @SM_Scenario2 @PGE @DR @DER
  Scenario: Adding DER to site w/rack of 2 for SM sim.
    Given http-client "denws_adapter"
    Given variables
      | bSiteName | DER_abc |
      | store_type         | DEN-STORE         |
      | batterySystem      | LG Chem 14S2 R600 |
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    And Payload:
    """
    {
      "id":null,
      "name":${bSiteName},
      "SID":null,
      "type":${store_type},
      "availablePower":995,
      "racks":[
        {"id":null,"name":"ESS Rack 1","rackNumber":1},
        {"id":null,"name":"ESS Rack 2","rackNumber":2}
      ],
      "maxDOD":95,
      "batterySystem":"${batterySystem}"
    }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK

#        {"id":null,"name":"ESS Rack 2","rackNumber":2},
#        {"id":null,"name":"ESS Rack 3","rackNumber":3},
#        {"id":null,"name":"ESS Rack 4","rackNumber":4},
#        {"id":null,"name":"ESS Rack 5","rackNumber":5},
#        {"id":null,"name":"ESS Rack 6","rackNumber":6},
#        {"id":null,"name":"ESS Rack 7","rackNumber":7},
#        {"id":null,"name":"ESS Rack 8","rackNumber":8},
#        {"id":null,"name":"ESS Rack 9","rackNumber":9},
#        {"id":null,"name":"ESS Rack 10","rackNumber":10}


  @SM_Scenario2 @PGE @DR @SOLAR
  Scenario: Adding solar cell to a location
    Given http-client "denws_adapter"
    Given variables
      | battery_store_name | SOLAR_abc  |
      | store_type         | SOLAR      |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    And Payload:
    """
      {
        "id":null,
        "SID":null,
        "name":${battery_store_name},
        "type":${store_type},
        "availablePower":null,
        "racks":[],
      }
    """
    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
    Then receives status 200 OK

#  @notworking
#  Scenario: uploading data file
#    Given http-client "denws_adapter"
#    Given variables
#      | filename | Twin Oaks Mercury DENOS Format.csv |
#      | startDt  | 2017-04-04T00:15:00.000Z   |
#      | endDt    | 2018-03-01T00:00:00.000Z   |
#      | interVal | 15                         |
#    And Content-Type: multipart/form-data
#    And Header Authorization: ${soak_bAuth}
#    And Payload:
#    """
#    {
#      "fileName":"${filename}",
#      "start":"${startDt}",
#      "end":"${endDt}"
#      "interval":"${interVal}"
#    }
#    """
#    When sends POST ${uRest}${uSite}/4e85ed14-fb85-4720-8c4c-939cde5fc27c/modeling/jobs/interval-data
#    Then receives status 200 OK

#
#  @notworking
#  Scenario: V: sales-modeling submission - fixed values - finished submission
#    Given http-client "denws_adapter"
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    When sends GET ${uRest}${uSite}/47bea4b8-bf28-4b91-bea3-88d533200bb3/modeling/jobs/interval-data/status
#    Then validate $[0].fileName is Boston_601_Take2_5min_Jan16.csv
#    Then validate $[0].status is finished
#    Then receives status 200 OK

#  @test @working @soak @verified
#  Scenario: oe-svc : posting sales-modeling job
#    Given http-client "denws_adapter"
#    Given variables
#      | sDate | 2018-06-01T00:00:00-07:00 |
#      | eDate | 2018-06-30T23:59:59-07:00 |
#      | l_l_v | true |
#      | d_d   | true |
#      | c_m   | false |
#      | m_m_d | false |
#      | m_d   | false |
#      | c_d   | false |
#      | b_e_c | true  |
#      | sup   | true  |
#      | z_s_p | true  |
#      | c_f_i | false |
#      | d_m_p | false |
#      | b_d | true |
#      | algorithm | cmaes |
#      | loadType | null |
#      | optFactor | 1 |
#      | capLossAdj | 0.15 |
#      | minImport | 0 |
#      | numControlParam | 3 |
#      | lBound | 0 |
#      | bSiteId | 4e85ed14-fb85-4720-8c4c-939cde5fc27c |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    And Payload:
#    """
#    {
#      "startDate":"${sDate}",
#      "endDate":"${eDate}",
#      "valueStreams":
#        {
#          "LOWER_LIMIT_VIOLATION":${l_l_v},
#          "DAILY_DEMAND":${d_d},
#          "CONTRACT_MANEUVER":${c_m},
#          "MODIFIED_MONTHLY_DEMAND":${m_m_d},
#          "MONTHLY_DEMAND":${m_d},
#          "CONTRACT_DEMAND":${c_d},
#          "BATTERY_ENERGY_CHANGE":${b_e_c},
#          "SUPPLY":${sup},
#          "ZERO_SLOPE_PREVENTION":${z_s_p},
#          "CAPACITY_FACTOR_INCENTIVE":${c_f_i},
#          "DEMAND_MANAGEMENT_PROGRAM":${d_m_p},
#          "BATTERY_DEGRADATION":${b_d}
#        },
#      "algorithm":"${algorithm}",
#      "loadType":${loadType},
#      "optimismFactor":${optFactor},
#      "capacityLossAdjustment":${capLossAdj},
#      "minImport":${minImport},
#      "numberOfControlParameters":${numControlParam},
#      "lowerBound":${lBound}
#    }
#    """
#    When sends POST /rest/sites/4e854-fb85-4720-8c4c-939cde5fc27c/modeling/jobs/optimization
#    Then receives status 404 NOT_FOUND
#    Then sleep 1500 ms
#    When sends POST /rest/sites//modeling/jobs/optimization
#    Then receives status 404 NOT_FOUND
#    Then sleep 1500 ms
#    When sends POST /rest/sites/%21%7E%40%5B%2e%23%5D%28%5F%24%29%2A%3A/modeling/jobs/optimization
#    Then receives status 500 INTERNAL_SERVER_ERROR
#    Then sleep 1500 ms
#    When sends POST /rest/sites/${bSiteId}/modeling/jobs/optimization
#    Then receives status 200 OK

#  @notworking @requiresExtractedData
#  Scenario: V: delete newly added solar cell, DER site w/ 10 racks and site used for SM
#    Given http-client "denws_adapter"
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    When DELETE rest/sites/${rack1SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack2SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack3SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack4SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack5SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack6SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack7SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack8SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack9SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${rack10SiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${solarSiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${derSiteId}
#    Then receives 200 OK
#    When DELETE rest/sites/${bSiteId}
#    Then receives 200 OK
