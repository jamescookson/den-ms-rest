Feature: Topology Service

  Background:
    Given variables
      | frDate   | 2018-01-01T01:01:01|
      | toDate   | 2019-12-31T23:56:04|
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |

  @regression
  Scenario Outline: <act> : <url> : <descr>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | act | adapt | url | code | msg | descr |
      | GET | topo    | /geolocations  | 200 | OK | geolocation-ok |
      | GET | topo    | /sites  | 200 | OK | sites-ok |
      | GET | topo    | /stores  | 200 | OK | stores-ok |
      | GET | topo    | /utilities  | 200 | OK | utilities-ok |
      | GET | topo    | /geolocate  | 404 | NOT_FOUND | failure-geolocation-badUrl |
      | GET | topo    | /site  | 404 | NOT_FOUND | failure-sites-badUrl |
      | GET | topo    | /store  | 404 | NOT_FOUND | failure-stores-badUrl |
      | GET | topo    | /utility  | 404 | NOT_FOUND | failure-utilities-badUrl |
      | GET | topo    | /geolocations/health  | 400 | BAD_REQUEST | failure-geolocation-wronghealth |
      | GET | topo    | /geolocations/info  | 400 | BAD_REQUEST | failure-geolocation-wronginfo |
      | GET | topo    | /sites/health  | 500 | INTERNAL_SERVER_ERROR | failure-sites-callnotallowed |
      | GET | topo    | /sites/info  | 500 | INTERNAL_SERVER_ERROR | failure-sites-callnotallowed |
      | GET | topo    | /stores/health  | 500 | INTERNAL_SERVER_ERROR | failure-stores-callnotallowed |
      | GET | topo    | /stores/info  | 500 | INTERNAL_SERVER_ERROR | failure-stores-callnotallowed |
      | GET | topo    | /utilities/health  | 400 | BAD_REQUEST | failure-utilities-wronghealth |
      | GET | topo    | /utilities/info  | 400 | BAD_REQUEST | failure-utilities-wronginfo |

  @regression
  Scenario Outline: <adapt> - <url> - querystring - failure cases, <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /geolocations |
      | query   | <devId> |
    And Accept: application/json
    When sends <act> <url><query>
    And receives status <code> <msg>

    Examples:
      | act | adapt | url | query | code | msg | descr |
      | GET | topo    | /geolocations | ?programId=12345678-1234-4123-a123-123456781012 | 200 | OK  | non-existent-programId |
      | GET | topo    | /geolocations | ?city=kalamazoo | 404 | NOT_FOUND  | non-existent-city-in-listing |
      | GET | topo    | /geolocations | ?programId= | 200 | OK  | programId-nonVital-null-entry-graceful-handling |
      | GET | topo    | /geolocations | ?programId=abc123d4 | 400 | BAD_REQUEST  | bad-programId |
      | GET | topo    | /geolocations | ?city= | 404 | NOT_FOUND  | vitalEntry-no-city-enetered |
      | GET | topo    | /geolocations | ?latitude=123.23 | 400 | BAD_REQUEST  | appropriate-handling-on-lat |
      | GET | topo    | /geolocations | ?longitude=123.23 | 400 | BAD_REQUEST  | appropriate-handling-on-long |
      | GET | topo    | /geolocations | ?LATITUDE=123.23&longitude=asbc | 400 | BAD_REQUEST | appropriate-handling-on-caps-bad-long |
      | GET | topo    | /geolocations | ?LONGITUDE=123.23&latitude=asdb | 400 | BAD_REQUEST | appropriate-handling-on-caps-bad-lat |
      | GET | topo    | /geolocations | ?latitude=123.23&longitude=asbc | 500 | INTERNAL_SERVER_ERROR | appropriate-handling-on-bad-long |
      | GET | topo    | /geolocations | ?longitude=123.23&latitude=asdb | 500 | INTERNAL_SERVER_ERROR | appropriate-handling-on-bad-lat |
      | GET | topo    | /geolocations | ?latitude= | 400 | BAD_REQUEST  | null-entry-for-lat |
      | GET | topo    | /geolocations | ?longitude= | 400 | BAD_REQUEST  | null-entry-for-long |
      | GET | topo    | /geolocations | ?latude=123.23 | 200 | OK | forgiveness-for-mispelling-querystring-lat |
      | GET | topo    | /geolocations | ?longude=123.23 | 200 | OK | forgiveness-for-mispelling-querystring-long |
      | GET | topo    | /geolocations | city=toronto | 404 | NOT_FOUND | mispelling-missing-?-querystring-city |
      | GET | topo    | /geolocations | ?program-id=1234567810-1233-4122-a122-123456781012 | 200 | OK | forgiveness-for-mispelling-querystring-programid |
      | GET | topo    | /geolocations | ?city=Toronto&program-id=1234567810-1233-4122-a122-123456781012 | 200 | OK | good-city-ignoring-bad-programId |
      | GET | topo    | /geolocations | ?city=Toronto&latitude=12.23 | 400 | BAD_REQUEST  | queryString-missing-required-notallowed |
      | GET | topo    | /geolocations | ?city=Toronto&longitude=12.23 | 400 | BAD_REQUEST  | queryString-missing-required-notallowed |

  @regression
  Scenario Outline: <act> Geolocations by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /geolocations |
      | locId   | <devId> |
    And Accept: application/json
    When sends <act> ${geo}/${locId}
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 400 | BAD_REQUEST | chars-in-id |
      | GET | topo | ${frDate} | 400 | BAD_REQUEST | 500-date-as-id |
      | GET | topo | ${badTxt} | 400 | BAD_REQUEST | 500-bad-text-id |
      | GET | topo | ${badChr} | 400 | BAD_REQUEST | 404-bad-char-id |
      | GET | topo | ${ise500} | 400 | BAD_REQUEST | 500-ise-text-id |
      | DELETE | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | DELETE | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | DELETE | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 400 | BAD_REQUEST | chars-in-id |
      | DELETE | topo | ${frDate} | 400 | BAD_REQUEST | 500-date-as-id |
      | DELETE | topo | ${badTxt} | 400 | BAD_REQUEST | 500-bad-text-id |
      | DELETE | topo | ${badChr} | 400 | BAD_REQUEST | 404-bad-char-id |
      | DELETE | topo | ${ise500} | 400 | BAD_REQUEST | 500-ise-text-id |

  @regression
  Scenario Outline: <act> Devices by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | device  | /devices |
      | devId   | <devId> |
    And Accept: application/json
    When sends <act> ${device}/${devId}/sid
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 500 | INTERNAL_SERVER_ERROR | chars-in-id |
      | GET | topo | ${frDate} | 500 | INTERNAL_SERVER_ERROR | 500-date-as-id |
      | GET | topo | ${badTxt} | 500 | INTERNAL_SERVER_ERROR | 500-bad-text-id |
      | GET | topo | ${badChr} | 404 | NOT_FOUND | 404-bad-char-id |
      | GET | topo | ${ise500} | 500 | INTERNAL_SERVER_ERROR | 500-ise-text-id |

  @regression
  Scenario Outline: <act> Utilities by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /utilities |
      | locId   | <devId> |
    And Accept: application/json
    When sends <act> ${geo}/${locId}
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 400 | BAD_REQUEST | chars-in-id |
      | GET | topo | ${frDate} | 400 | BAD_REQUEST | 500-date-as-id |
      | GET | topo | ${badTxt} | 400 | BAD_REQUEST | 500-bad-text-id |
      | GET | topo | ${badChr} | 400 | BAD_REQUEST | 404-bad-char-id |
      | GET | topo | ${ise500} | 400 | BAD_REQUEST | 500-ise-text-id |
      | DELETE | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | DELETE | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | DELETE | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 400 | BAD_REQUEST | chars-in-id |
      | DELETE | topo | ${frDate} | 400 | BAD_REQUEST | 500-date-as-id |
      | DELETE | topo | ${badTxt} | 400 | BAD_REQUEST | 500-bad-text-id |
      | DELETE | topo | ${badChr} | 400 | BAD_REQUEST | 404-bad-char-id |
      | DELETE | topo | ${ise500} | 400 | BAD_REQUEST | 500-ise-text-id |

  @regression
  Scenario Outline: <act> Sites by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /sites |
      | locId   | <devId> |
    And Accept: application/json
    When sends <act> ${geo}/${locId}
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 500 |INTERNAL_SERVER_ERROR | chars-in-id |
      | GET | topo | ${frDate} | 500 |INTERNAL_SERVER_ERROR | 500-date-as-id |
      | GET | topo | ${badTxt} | 500 |INTERNAL_SERVER_ERROR | 500-bad-text-id |
      | GET | topo | ${badChr} | 500 |INTERNAL_SERVER_ERROR | 500-bad-char-id |
      | GET | topo | ${ise500} | 500 |INTERNAL_SERVER_ERROR | 500-ise-text-id |

  @regression
  Scenario Outline: <act> Sites/Stores by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /sites |
      | locId   | <devId> |
    And Accept: application/json
    When sends <act> ${geo}/${locId}/stores
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 200 | OK | forgiveness-non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 200 | OK | forgiveness-deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 500 |INTERNAL_SERVER_ERROR | chars-in-id |
      | GET | topo | ${frDate} | 500 |INTERNAL_SERVER_ERROR | 500-date-as-id |
      | GET | topo | ${badTxt} | 500 |INTERNAL_SERVER_ERROR | 500-bad-text-id |
      | GET | topo | ${badChr} | 500 |INTERNAL_SERVER_ERROR | 500-bad-char-id |
      | GET | topo | ${ise500} | 500 |INTERNAL_SERVER_ERROR | 500-ise-text-id |

  @regression
  Scenario Outline: <act> Stores by ID, failure cases - <descr>
    Given http-client "<adapt>_adapter"
    Given variables
      | geo  | /stores |
      | locId   | <devId> |
    And Accept: application/json
    When sends <act> ${geo}/${locId}
    And receives status <code> <msg>

    Examples:
      | act | adapt | devId | code | msg | descr |
      | GET | topo | 12345678-1234-4123-a123-123456781012 | 404 | NOT_FOUND | non-existent-id |
      | GET | topo | 0081c63c-0e81-4ae6-9fb0-580a3559fb67 | 404 | NOT_FOUND | deleted-id |
      | GET | topo | 12%20%20c63c-0e81-4ae6-9fb0-580a3559fb67 | 500 |INTERNAL_SERVER_ERROR | chars-in-id |
      | GET | topo | ${frDate} | 500 |INTERNAL_SERVER_ERROR | 500-date-as-id |
      | GET | topo | ${badTxt} | 500 |INTERNAL_SERVER_ERROR | 500-bad-text-id |
      | GET | topo | ${badChr} | 500 |INTERNAL_SERVER_ERROR | 500-bad-char-id |
      | GET | topo | ${ise500} | 500 |INTERNAL_SERVER_ERROR | 500-ise-text-id |

  @regression
  Scenario Outline: <act> utilities, lseId cases - <note>
    Given http-client "<svc>_adapter"
    And Accept: application/json
    When sends <act> <url>
    And receives status <code> <msg>

    Examples:
      | svc | act | url | code | msg | note |
      | topo | GET | /utilities?lseId=100336 | 200 | OK | get-SINGLE-lseId-Querystring |
      | topo | GET | /utilities?lseId=100336&lseId=100389&lseId=1043 | 200 | OK | get-MANY-lseId-Querystring |
      | topo | GET | /utilities?lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337 | 200 | OK | get-MAXLENGTH-lseId-Querystring |
      | topo | GET | /utilities?lseId=true | 200 | OK | forgiveness-badLseId-Querystring |
      | topo | GET | /utilities?lseId=,.':;-+=@#~!*[]()_ | 200 | OK | forgiveness-charsForLseId-Querystring |

  @regression
  Scenario Outline: Add geolocation, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | active | <act> |
      | city | <city> |
      | id | <id> |
      | latitude | <lat> |
      | longitude | <lon> |
      | programId | <programId> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "active":"${active}",
      "city":"${city}",
      "id":"${id}",
      "latitude":${latitude},
      "longitude":"${longitude}",
      "programId":"${programId}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | act | city | id | lat | lon | programId | note |
      | topo | /geolocations | 400 | BAD_REQUEST | hello | Toronto | devSiteGru | 12.23 | 12.23 | 123456 | bad-active-state-text |
      | topo | /geolocations | 400 | BAD_REQUEST | []_+=, | Toronto | devSiteGru | 12.23 | 12.23 | 123456 | bad-active-state-chars |
      | topo | /geolocations | 400 | BAD_REQUEST |  | Toronto | devSiteGru | 12.23 | 12.23 | 123456 | missing-active-state |
      | topo | /geolocations | 400 | BAD_REQUEST | false |  | devSiteGru | 12.23 | 12.23 | 123456 | missing-city |
      | topo | /geolocations | 400 | BAD_REQUEST | false | []_+=, | devSiteGru | 12.23 | 12.23 | 123456 | chars-city |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru |  | 12.23 | 123456 | missing-lat |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 |  | 123456 | missing-long |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | hello | 12.23 | 123456 | bad-lat |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 | hello | 123456 | bad-long |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | []_+=, | 12.23 | 123456 | chars-lat |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 | []_+=, | 123456 | chars-long |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | true | 12.23 | 123456 | bool-lat |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 | false | 123456 | bool-long |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | []_+=, | 12.23 | 12.23 | 123456 | bad-id |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 | 12.23 | []_+=, | bad-program-id |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | true | 12.23 | 12.23 | 123456 | bool-id |
      | topo | /geolocations | 400 | BAD_REQUEST | false | Toronto | devSiteGru | 12.23 | 12.23 | true | bool-program-id |

  @regression
  Scenario Outline: Add utility, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <name> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "name":"${name}",
      "shortName":"${shortName}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | name | short | note |
      | topo | /utilities | 409 | CONFLICT | 12345678-1234-4123-a123-123456781012 | 1043 | devSiteGru | devSiteGru | duplicate-lseId |
      | topo | /utilities | 400 | BAD_REQUEST |  |  |  |  | empty-values-not-allowed |

  #note difference between id & any other field in payload
  @regression
  Scenario Outline: Add utility - manipulate-ID, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <name> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":${id},
      "lseId":"${lse}",
      "name":"${name}",
      "shortName":"${shortName}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | name | short | note |
      | topo | /utilities | 400 | BAD_REQUEST | 12345678-1234-4123-a123-123456781012 | []_+=, | devSiteGru | devSiteGru | chars-asId |
      | topo | /utilities | 400 | BAD_REQUEST | 12345678-1234-4123-a123-123456781012 | true | devSiteGru | devSiteGru | boolean-asId |
      | topo | /utilities | 400 | BAD_REQUEST | 12345678-1234-4123-a123-123456781012 | 123456 | devSiteGru | devSiteGru | number-asId |

  #note difference between name  & any other field in payload
  @regression
  Scenario Outline: Add utility - manipulate-Name, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <name> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "name":${name},
      "shortName":"${shortName}"
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | name | short | note |
      | topo | /utilities | 400 | BAD_REQUEST | 12345678-1234-4123-a123-123456781012 | 123456 | []_+=, | devSiteGru | chars-badName |

  #note difference between shortName  & any other field in payload
  @regression
  Scenario Outline: Add utility - manipulate-shortName, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <name> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "name":"${name}",
      "shortName":${shortName}
    }
    """
    When sends POST ${url}
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | name | short | note |
      | topo | /utilities | 400 | BAD_REQUEST | 12345678-1234-4123-a123-123456781012 | 123456 | devSiteGru | []_+=, | chars-badShortName |

  @regression
  Scenario Outline: Update utility, failure cases, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <name> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "name":"${name}",
      "shortName":"${shortName}"
    }
    """
    When sends PUT ${url}/<otherId>
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | name | short | note | otherId |
      | topo | /utilities | 404 | NOT_FOUND | 12345678-1234-4123-a123-123456781012 | 123456 | devSiteGru | devSiteGru | failure-nonId-forbothIds | 12345678-1234-4123-a123-123456781012 |
      | topo | /utilities | 404 | NOT_FOUND | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 123456 | devSiteGru | devSiteGru | failure-goodIdInside-URLhas-nonExistentId | 12345678-1234-4123-a123-123456781012 |
      | topo | /utilities | 400 | BAD_REQUEST |  |  |  |  | failure-no-entries | 12345678-1234-4123-a123-123456781012 |
      | topo | /utilities | 400 | BAD_REQUEST | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 |  | TORuston | failure-removes-name | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 400 | BAD_REQUEST | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston |  | failure-removes-shortName | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston | TORuston | accepted-no-change-to-entry | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK |  | 1043 | Town of Ruston | TORuston | forgiveness-no-change-to-entry | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | []_+=, | TORuston | chars-for-name | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | true | TORuston | bool-for-name | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston | []_+=, | chars-for-shortName | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston | false | bool-for-shortName | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |  | Town of Ruston | TORuston | removes-lseId | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | true | Town of Ruston | TORuston | sets-to-true | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | topo | /utilities | 200 | OK | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston | TORuston | reset-origin | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |

  @regression
  Scenario Outline: Update utility, removing Name - failure case, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | shortName | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "shortName":"${shortName}"
    }
    """
    When sends PUT ${url}/<otherId>
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | short | note | otherId |
      | topo | /utilities | 400 | BAD_REQUEST | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | TORuston | failure-required-field | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |

  @regression
  Scenario Outline: Update utility, removing Short Name - failure case, <code> <msg> <note>
    Given http-client "<adapt>_adapter"
    Given variables
      | bSiteId  | 9f1612ad-7b41-4fe9-9a67-0e3a25708611 |
      | bAuth   | Basic YWJjMTIzOmJkMzkwM2FlLTRkNDgtNDc4MC1hMzNhLTZkY2FjMzg0NDk0NQ== |
      | url | <url> |
      | id | <id> |
      | lse | <lse> |
      | name | <short> |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":"${id}",
      "lseId":"${lse}",
      "name":"${name}"
    }
    """
    When sends PUT ${url}/<otherId>
    Then receives status <code> <msg>

    Examples:
      | adapt | url | code | msg | id | lse | short | note | otherId |
      | topo | /utilities | 400 | BAD_REQUEST | 10f972fc-d541-44bf-b7c4-5e5e157c9994 | 1043 | Town of Ruston | failure-required-field | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |

  @failing
  #--from HttpStatus.java
  #  /**
  #    * {@code 431 Request Header Fields Too Large}.
  #    * @see <a href="http://tools.ietf.org/html/rfc6585#section-5">Additional HTTP Status Codes</a>
  #    */
  #  REQUEST_HEADER_FIELDS_TOO_LARGE(431, "Request Header Fields Too Large"),
 #--this case SHOULD pass -- but something about framework and way it is handling the request prevents this
  Scenario: test utilities /lseId=<long string>, 431 case
    Given http-client "topo_adapter"
    And Accept: application/json
    When sends GET /utilities?lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100338&lseId=100336&lseId=100337&lseId=100336&lseId=100337&lseId=100389&lseId=100336&lseId=100336&lseId=100336&lseId=100337&lseId=100389&lseId=100389&lseId=100336&lseId=100337&lseId=100389&lseId=100336&lseId=100337&lseId=100389&lseId=100336&lseId=100337&lseId=100389&lseId=100389&lseId=100389&lseId=100389
    And receives status 431 REQUEST_HEADER_FIELDS_TOO_LARGE