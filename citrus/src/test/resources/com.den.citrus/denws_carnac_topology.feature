Feature: rest endpoints, carnac-tornado-topo

  Background:
    Given variables
      | utilUrl  | /utility |
      | utixUrl  | /utilities |
      | msgUrl   | /messages  |
      | badTxt   | abcdef        |
      | badInt   | 1123456       |
      | badUuid1  | 2aaaaaaa-4ccc-4ccc-b111-aaaabbbbdddd |
      | badUuid2 | 2aaaa-4ccc-4ccc-b111-aaaabbbbdddd    |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |
      | badChr1   | %21%7E%40%5B%2e%23%5D%28%5F%24%29%2A%3A |
      | badChr2 | %7B%5B%E2%80%A6?%5D%7D |
      | badTxtX | hello_dolly            |

  @regression @soak
  Scenario: Topo, GET utilities
    Given http-client "topo_adapter"
    When sends GET /utilities
    And receives status 200 OK
    When sends GET /utilities?lseId=2252
    Then validate $[0].name is Consolidated Edison Co-NY Inc
    Then validate $[0].id is abf409b8-6509-4dec-8e14-13b8dca030d1
    And receives status 200 OK

  @regression @soak
  Scenario: Topo, GET utilities/<ID>
    Given http-client "topo_adapter"
    When sends GET /utilities/abf409b8-6509-4dec-8e14-13b8dca030d1
    Then validate $.name is Consolidated Edison Co-NY Inc
    And receives status 200 OK

  @regression @soak
  Scenario Outline: <adapt> <type> <descr> , expect <code>
    Given http-client "<adapt>_adapter"
    And Accept: application/json
    When sends <type> <url>
    And receives status <code> <msg>

    Examples:
      | adapt | type | url | code | msg | descr |
      | topo  | GET  | /utilities/abf4 | 400 | BAD_REQUEST | somethingISoff-0 |
      | topo  | GET  | /utilities/1234 | 400 | BAD_REQUEST | somethingISoff-1 |
      | topo  | GET  | /utilities/%21%7E%40%5B%2e%23%5D%28%5F%24%29%2A%3A | 400 | BAD_REQUEST | somethingISoff-2 |
      | topo  | GET  | /utilities/2aaaaaaa-4ccc-4ccc-b111-aaaabbbbdddd | 404 | NOT_FOUND | somethingISoff-3 |
      | topo  | GET  | /utilities/2aaaa-4ccc-4ccc-b111-aaaabbbbdddd | 404 | NOT_FOUND | somethingISoff-4 |
      | tornado | DELETE | ${msgUrl}/${badTxt} | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | DELETE | ${msgUrl}/${badInt} | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | DELETE | ${msgUrl}/2aaaaaaa- | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | DELETE | ${msgUrl}/${badUuid1} | 500 | INTERNAL_SERVER_ERROR | messages/<ID> failure calls |
      | tornado | DELETE | ${msgUrl}/${badUuid2} | 500 | INTERNAL_SERVER_ERROR | messages/<ID> failure calls |
      | tornado | DELETE | ${msgUrl}/${badChr1} | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | GET | ${msgUrl}/abf4 | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | GET | ${msgUrl}/1234 | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | tornado | GET | ${msgUrl}/${badUuid1} | 404 | NOT_FOUND | messages/<ID> failure calls |
      | tornado | GET | ${msgUrl}/${badUuid2} | 404 | NOT_FOUND | messages/<ID> failure calls |
      | tornado | GET | ${msgUrl}/${badChr1} | 400 | BAD_REQUEST | messages/<ID> failure calls |
      | topo | DELETE | ${utixUrl}/${badChr1} | 400 | BAD_REQUEST | utilities/<ID> failure calls |
      | topo | DELETE | ${utixUrl}/${badChr2} | 400 | BAD_REQUEST | utilities/<ID> failure calls |
      | topo | DELETE | ${utixUrl}/${badUuid1} | 404 | NOT_FOUND | utilities/<ID> failure calls |
      | topo | DELETE | ${utixUrl}/${badUuid2} | 404 | NOT_FOUND | utilities/<ID> failure calls |
      | topo | DELETE | ${utixUrl}/${badChr1} | 400 | BAD_REQUEST | utilities/<ID> failure calls |
      | carnac | GET | ${utilUrl}/abf4 | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | carnac | GET | ${utilUrl}/1234 | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | carnac | GET | ${utilUrl}/${badUuid1} | 404 | NOT_FOUND | utility/<ID> failure calls |
      | carnac | GET | ${utilUrl}/${badUuid2} | 404 | NOT_FOUND | utility/<ID> failure calls |
      | carnac | GET | ${utilUrl}/${badChr1} | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | carnac | DELETE | ${utilUrl}/${badTxt} | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | carnac | DELETE | ${utilUrl}/${badInt} | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | carnac | DELETE | ${utilUrl}/${badUuid1} | 404 | NOT_FOUND | utility/<ID> failure calls |
      | carnac | DELETE | ${utilUrl}/${badUuid2} | 404 | NOT_FOUND | utility/<ID> failure calls |
      | carnac | DELETE | ${utilUrl}/${badChr1} | 400 | BAD_REQUEST | utility/<ID> failure calls |
      | tornado | GET | ${utixUrl}/abf4 | 404 | NOT_FOUND | utilities/<ID> failure calls |
      | tornado | GET | ${utixUrl}/1234 | 404 | NOT_FOUND | utilities/<ID> failure calls |
      | tornado | GET | ${utixUrl}/${badUuid1} | 404 | NOT_FOUND | utilities/<ID> failure calls, expect 404 |
      | tornado | GET | ${utixUrl}/${badUuid2} | 404 | NOT_FOUND | utilities/<ID> failure calls, expect 404 |
      | tornado | GET | ${utixUrl}/${badChr2} | 404 | NOT_FOUND | utilities/<ID> failure calls, expect 400 |
