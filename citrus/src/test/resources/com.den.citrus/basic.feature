Feature: Sample Citrus Test

  @CucumberOptions
  Scenario: Test health endpoint for service running on port specified in citrus-application.properties
    Given http-client "httpClient"
    When sends GET /health
    Then validate $.status is UP
    And receives status 200 OK

