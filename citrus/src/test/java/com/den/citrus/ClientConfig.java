package com.den.citrus;

import com.consol.citrus.annotations.CitrusEndpoint;
import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.http.client.HttpClientBuilder;
import com.consol.citrus.junit.CitrusJUnit4Runner;
import com.consol.citrus.testng.CitrusParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("citrus-application.properties")
public class ClientConfig {
    private static Logger log = LoggerFactory.getLogger(ClientConfig.class);

//    @Bean
//    public DockerClient docker() {
//        return CitrusEndpoints.docker()
//                .client()
//                .url("tcp://127.0.0.1:1234")
//                .build();
//    }

    // For all adapters host and port are specified in the citrus-application.properties
    @Bean
    public HttpClient httpClient(@Value("${sample.host:localhost}") String adapterHost, @Value("${sample.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient dr_adapter(@Value("${dr.host:localhost}") String adapterHost, @Value("${dr.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }


    @Bean
    public HttpClient rate_adapter(@Value("${rate.host:localhost}") String adapterHost, @Value("${rate.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient capture_adapter(@Value("${capture.host:localhost}") String adapterHost, @Value("${capture.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient stats_adapter(@Value("${stats.host:localhost}") String adapterHost, @Value("${stats.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient oe_adapter(@Value("${oe.host:localhost}") String adapterHost, @Value("${oe.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient premo_adapter(@Value("${premo.host:localhost}") String adapterHost, @Value("${premo.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient event_adapter(@Value("${event.host:localhost}") String adapterHost, @Value("${event.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient vpp_adapter(@Value("${vp.host:localhost}") String adapterHost, @Value("${vp.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient forecast_adapter(@Value("${forecast.host:localhost}") String adapterHost, @Value("${forecast.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient carnac_adapter(@Value("${carnac.host:localhost}") String adapterHost, @Value("${carnac.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient tornado_adapter(@Value("${tornado.host:localhost}") String adapterHost, @Value("${tornado.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient dayahead_adapter(@Value("${dayahead.host:localhost}") String adapterHost, @Value("${dayahead.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient weather_adapter(@Value("${weather.host:localhost}") String adapterHost, @Value("${weather.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient derp_adapter(@Value("${derp.host:localhost}") String adapterHost, @Value("${derp.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient senderp_adapter(@Value("${senderp.host:localhost}") String adapterHost, @Value("${senderp.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient billCalc_adapter(@Value("${billingCalcSvc.host:localhost}") String adapterHost, @Value("${billingCalcSvc.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient topo_adapter(@Value("${topo.host:localhost}") String adapterHost, @Value("${topo.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient eventsvc_adapter(@Value("${eventsvc.host:localhost}") String adapterHost, @Value("${eventsvc.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient denws_adapter(@Value("${denws.host:localhost}") String adapterHost, @Value("${denws.port:8080}") String adapterPort) {
        String url = String.format("https://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient secure_adapter(@Value("${secure.host:localhost}") String adapterHost, @Value("${secure.port:443}") String adapterPort) {
        String url = String.format("https://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient slack_adapter(@Value("${slack.host:localhost}") String adapterHost, @Value("${slack.port:443}") String adapterPort) {
        String url = String.format("https://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient eventrecorder_adapter(@Value("${eventrecorder.host:localhost}") String adapterHost, @Value("${eventrecorder.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient forecastms_adapter(@Value("${forecastms.host:localhost}") String adapterHost, @Value("${forecastms.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient msconfig_adapter(@Value("${msconfig.host:localhost}") String adapterHost, @Value("${msconfig.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }

    @Bean
    public HttpClient denmstest_adapter(@Value("${denmstest.host:localhost}") String adapterHost, @Value("${denmstest.port:8080}") String adapterPort) {
        String url = String.format("http://%s:%s", adapterHost, adapterPort);
        log.info("REST :: Calling {} with created adapter." , url);
        return CitrusEndpoints.http()
                .client()
                .requestUrl(url)
                .build();
    }
}
