package com.den.citrus;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.exceptions.InvalidFunctionUsageException;
import com.consol.citrus.functions.Function;


import java.util.List;

public class IntegerConversion implements Function {
    @Override
    public String execute(List<String> parameterList, TestContext testContext) {
        if(parameterList.size() != 1 ) {
            throw new InvalidFunctionUsageException("IntegerConversion requires 1 arguments");
        }
        try {
            Double value = Double.parseDouble(parameterList.get(0));
            return String.valueOf(value.intValue());
        } catch (NumberFormatException e) {
            throw new InvalidFunctionUsageException("IntegerConversion requires a numeric argument", e);
        }
    }
}
