package com.den.citrus;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        glue = {
                "com.consol.citrus.cucumber.step.runner.core",
                "com.consol.citrus.cucumber.step.runner.http",
                "com.consol.citrus.cucumber.step.runner.docker",
                "com.den.citrus",
        },
        plugin = {
                "com.consol.citrus.cucumber.CitrusReporter",
        },
        features = {
                "src/test/resources",
        }
)
public class CitrusIT {

}
