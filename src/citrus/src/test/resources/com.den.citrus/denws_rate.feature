Feature:  Rate REST endpoints

  Background:
    Given variables
      | uRest   | /rest |
      | uSite   | /sites  |
      | uStore  | /stores |
      | uRate   | /rates  |
      | rstUrl  | /rest   |
      | metUrl  | /meta   |
      | cfgUrl  | /config |
      | tarUrl  | /tariffs  |
      | sUrl    | /schedules|
      | uAcct   | /accounts |
      | vUrl    | /version  |
      | utlUrl  | /utilities|
      | dayUrl  | /dayahead |
      | tariffCde|  CSTE19  |
      | gString  | group_id |
      | tString  | tariffId |
      | utlString| utilityId|
      | zString  | zipcode  |
      | qeSiteNm | TestSite1  |
      | qeStorNm | Battery1   |
      | qeRak1Nm | ESS Rack 1 |
      | qeRak2Nm | ESS Rack 2 |
      | tarCde   | E19      |
      | zipCde   | 98466    |
      | tarId    | 3161243  |
      | ratCde   | CSTE19   |
      | charStr  | %21%7E%40%5B%2e%23%5D%28%5F%24%29%2A%3A |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |
      | dateStr  | 2018-11-05T23:56:04|
      | charStr2 | %7B%5B%E2%80%A6?%5D%7D |
      | textStr  | qwertyuiopasdfghjkl |
      | utilId   | 10f972fc-d541-44bf-b7c4-5e5e157c9994 |
      | qeStore | 768b8574-d513-4b48-a5cc-1401808078ea  |
      | qeDvcId | 672cb9a6-b742-4c95-8388-648c9358f771  |
      | qeStrId | 88b2c7a1-435c-45b2-8a74-20653aac221a  |
      | qeRak1  | a6d41e60-680e-4dcd-8ce0-d04d69d7add4  |
      | qeRak2  | 952e8e00-7b34-44ca-ba24-fd0b09fe3a24  |
      | qeStoreSiteId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
      | bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
      | longStr  | 012345678100123456781001234567810012345678100123456785001234567810012345678100123456781001234567810012345671000123456781001234567810012345678100123456781001234567850012345678100123456781001234567810012345678100123456720001234567810012345678100123456781001234567810012345678500123456781001234567810012345678100123456781001234567300 |

  @regression
  Scenario: V : rate-capture-svc endpoint - specific ID, XML retuened : tariffs/<ID>
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
    When sends GET ${tarUrl}/${tarId}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoint - all configs : tariffs/configs
    Given http-client "capture_adapter"
    When sends GET ${tarUrl}/configs
    Then receives status 200 OK

  @regression
  Scenario: V: rate-svc plan by ZipCode : Info specific to a known rate
    Given http-client "rate_adapter"
    Given variables
      | zIp     | 98406 |
      | rCode   | CNFL_Supply_usd |
      | rName   | CNFL_TMT_Supply US$ |
      | uName   | Instituto Costarricense de Electricidad |
      | uId     | 68913276-27be-4387-bb66-c690899c7546    |
      | tType   | Supply                                  |
      | eXp     | never                                   |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}?${zString}=${zIp}
    Then validate $[0].code is ${rCode}
    Then validate $[0].name is ${rName}
    Then validate $[0].utilityName is ${uName}
    Then validate $[0].utilityId is ${uId}
    Then validate $[0].tariffType is ${tType}
    Then validate $[0].expires is ${eXp}
    Then receives status 200 OK

  @requiresfix
  Scenario: V : rate-capture-svc endpoints : tariffs/<ID>/rates : Specific data
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | tName  | Residential |
      | tCode  | None_Residential |
      | rGroup1 | Customer Charge |
      | rGroup2 | Minimum Charge |
      | rGroup3 | Energy Charge |
      | rChgType1 | FIXED_PRICE |
      | rChgType2 | MINIMUM |
      | rChgType3 | CONSUMPTION_BASED |
      | predict1  | fixedMonthly      |
      | predict2  | Ignore            |
      | predict3  | MonthlyEnergyRates|
      | season    | Summer            |
      | sStrtEnd  | 4/1 - 9/11        |
    When sends GET ${tarUrl}/${tarId}${uRate}
    Then validate $.masterTariffId is ${mTarId}
    Then validate $.tariffName is ${tName}
    Then validate $.tariffCode is ${tCode}
    Then validate $.rates[0].rateGroupName is ${rGroup1}
    Then validate $.rates[0].rateName is ${rGroup1}
    Then validate $.rates[0].chargeType is ${rChgType1}
    Then validate $.rates[0].prediction is ${predict1}
    Then validate $.rates[0].seasonName is ${season}
    Then validate $.rates[0].seasonStartEnd is ${sStrtEnd}
    Then validate $.rates[1].rateGroupName is ${rGroup2}
    Then validate $.rates[1].rateName is ${rGroup2}
    Then validate $.rates[1].chargeType is ${rChgType2}
    Then validate $.rates[1].prediction is ${predict2}
    Then validate $.rates[1].seasonName is ${season}
    Then validate $.rates[1].seasonStartEnd is ${sStrtEnd}
    Then validate $.rates[2].rateGroupName is ${rGroup3}
    Then validate $.rates[2].rateName is ${rGroup3}
    Then validate $.rates[2].chargeType is ${rChgType3}
    Then validate $.rates[2].prediction is ${predict3}
    Then validate $.rates[2].seasonName is ${season}
    Then validate $.rates[2].seasonStartEnd is ${sStrtEnd}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoints : tariffs/<ID>/config : specific config
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
    When sends GET ${tarUrl}/${tarId}${cfgUrl}
    Then validate $.masterTariffId is ${mTarId}
    Then validate $.code is ${tCode}
    Then validate $.lseId is ${lseId}
    Then validate $.utility is ${uTil}
    Then validate $.smart-meter-interval is 12
    Then validate $.demand-calc-interval is 2
    Then sends GET ${tarUrl}/${tarId}/autoconfig
    Then receives status 200 OK

  # setup a common variations on the types given, however - parent utility is con-ed
  @regression @flavors @bug
  Scenario: V : rate-capture-svc endpoint - flavor based : tariffs/<ID>/<flavor designate>
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
      | flavDesignate1  | Service%20Type  |
      | flavDesignate2  | Territory     |
      | flavDesignate3  | Metering%20Service%20Charge  |
      | flav1  | High%20Tension     |
      | flav2  | Low%20Tension      |
      | flav3  | true   |
      | flav4  | false  |
      | flav5  | Zone%20H           |
      | flav6  | Zone%20I           |
      | flav7  | Zone%20J           |
      | requestFlavor1 | ${flavDesignate1}-${flav1} |
      | requestFlavor2 | ${flavDesignate1}-${flav2} |
      | requestFlavor3 | ${flavDesignate2}-${flav3} |
      | requestFlavor4 | ${flavDesignate2}-${flav4} |
      | requestFlavor5 | ${flavDesignate3}-${flav5} |
      | requestFlavor6 | ${flavDesignate3}-${flav6} |
      | requestFlavor7 | ${flavDesignate3}-${flav7} |
    When sends GET ${tarUrl}/${rateId}/${requestFlavor1}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor2}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor3}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor4}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor5}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor6}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor7}
    Then receives status 200 OK

  # these are designed to fail, but until resolved will be set for passing
  @regression @flavors @bug @negative
  Scenario: V : rate-capture-svc endpoint - flavor based : tariffs/<ID>/<flavor designate>
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3161243 |
      | tCode  | None_Residential |
      | lseId  | 1043             |
      | uTil   | Town of Ruston   |
      | flavDesignate1  | Service%20Type  |
      | flavDesignate2  | Territory     |
      | flavDesignate3  | Metering%20Service%20Charge  |
      | flav1  | High%20Tension     |
      | flav2  | Low%20Tension      |
      | flav3  | true   |
      | flav4  | false  |
      | flav5  | Zone%20H           |
      | flav6  | Zone%20I           |
      | flav7  | Zone%20J           |
      | requestFlavor0 | ${flav1}-${flavDesignate3} |
      | requestFlavor8 | ${flavDesignate3}-${flav7} |
      | requestFlavor9 | ${flavDesignate1}-${flav5} |
      | requestFlavorA | ${flavDesignate2}-${flav4} |
      | requestFlavorB | ${flavDesignate1}-${flav6} |
    When sends GET ${tarUrl}/${rateId}/${requestFlavor8}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor9}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavorA}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavorB}
    Then receives status 200 OK
    When sends GET ${tarUrl}/${rateId}/${requestFlavor0}
    Then receives status 200 OK


  # specific to master rate ID 3161243, returns custom made rate, direct verification to follow
  @regression
  Scenario: V : rate-capture-svc endpoint - raw data for available configs on <ID> : tariffs/<ID>/raw
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3306911 |
      | raW    | /raw    |
    When sends GET ${tarUrl}/${mTarId}${raW}
    Then validate $.type is Tariff
    Then validate $.results[0].tariffId is ${rateId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then receives status 200 OK

  @regression
  Scenario: V : rate-capture-svc endpoint - raw data for 3306911 - Town of Ruston
    Given http-client "capture_adapter"
    Given variables
      | mTarId | 3161243 |
      | rateId | 3306911 |
      | raW    | /raw    |
    When sends GET ${tarUrl}/${mTarId}${raW}
    Then validate $.type is Tariff
    Then validate $.results[0].tariffId is ${rateId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then validate $.results[0].masterTariffId is ${mTarId}
    Then validate $.results[0].tariffCode is None
    Then validate $.results[0].tariffName is Residential
    Then validate $.results[0].lseId is 1043
    Then validate $.results[0].lseName is Town of Ruston
    Then validate $.results[0].priorTariffId is ${mTarId}
    Then validate $.results[0].tariffType is DEFAULT
    Then validate $.results[0].customerClass is RESIDENTIAL
    Then validate $.results[0].customerCount is 494
    Then validate $.results[0].territoryId is 1151
    Then validate $.results[0].effectiveDate is 2018-04-01
    Then validate $.results[0].timeZone is US/Pacific
    Then validate $.results[0].billingPeriod is MONTHLY
    Then validate $.results[0].currency is USD
    Then validate $.results[0].chargeTypes is FIXED_PRICE,CONSUMPTION_BASED,MINIMUM
    Then validate $.results[0].chargePeriod is MONTHLY
    Then validate $.results[0].hasTimeOfUseRates is false
    Then validate $.results[0].hasTieredRates is false
    Then validate $.results[0].hasContractedRates is false
    Then validate $.results[0].hasRateApplicability is false
    Then validate $.results[0].isActive is true
    Then receives status 200 OK

#  @regression @test @needstesting
#  Scenario: Add/Update a rate schedule
#    Given http-client "rate_adapter"
#    And Accept: application/json
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#    {
#      "name":"Town of Ruston",
#      "lseId":1043,
#      "shortName":"TORuston"
#    }
#    """
#    When sends PUT ${sUrl}/${tariffCde}
#    Then receives status 200 OK

#  @regression @test @needstesting
#  Scenario: Add a utility for a customer via API
#    Given http-client "topo_adapter"
#    And Accept: application/json
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#    {
#      "name":"Town of Ruston",
#      "lseId":1043,
#      "shortName":"TORuston"
#    }
#    """
#    When sends POST ${utlUrl}
#    Then receives status 200 OK

#  Scenario Outline: Creating item necessary to setup site for SalesOps to show off to potential customers.
#Then sets variable <> to new value <>
#
#  Need: figure out how to insert value from path into variable
#
#  @regression @working @test
#  Scenario: Adding a DER location (withing DEN.OS Cloud >> Demand Energy >> QE)
#    Given http-client "denws_adapter"
#    Given variables
#      | newLocName | TestSite123 |
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    When sends POST ${rstUrl}${uAcct}/${qeStore}${uSite}/${newLocName}
#    Then receives status 200 OK
#    Then extract value $.id and insert into ${bSiteId}
#
#  CREATED be7f4250-5165-484c-9596-26fbc9782fc7 using ^^ & used for succeeding cases below
#
#  @regression @working @test
#  Scenario: Adding DER battery store with 2 racks to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 9cab06ee-70d7-4223-b46c-2bf252296f44 |
#      | battery_store_name | test_123_battery_site  |
#      | store_type         | DEN-STORE                 |
#      | battery_rack1_name | test_123_rack1            |
#      | battery_rack2_name | test_123_rack2            |
#      | battery_rack3_name | test_123_rack3            |
#      | battery_rack4_name | test_123_rack4            |
#      | battery_rack5_name | test_123_rack5            |
#      | battery_rack6_name | test_123_rack6            |
#      | battery_rack7_name | test_123_rack7            |
#      | batterySystem      | Enersys                   |
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":105,
#        "racks":[
#          {"id":null,"name":"${battery_rack1_name}","rackNumber":1},
#          {"id":null,"name":"${battery_rack2_name}","rackNumber":2},
#          {"id":null,"name":"${battery_rack3_name}","rackNumber":3},
#          {"id":null,"name":"${battery_rack4_name}","rackNumber":4},
#          {"id":null,"name":"${battery_rack5_name}","rackNumber":5},
#          {"id":null,"name":"${battery_rack6_name}","rackNumber":6},
#          {"id":null,"name":"${battery_rack7_name}","rackNumber":7}
#        ],
#        "maxDOD":65,
#        "batterySystem":${batterySystem}
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK

#    Then extract value $.id and insert into ${bSiteId}
#    Then extract value $.racks[0].id and insert into ${bSiteRackId_1}
#    Then extract value $.racks[1].id and insert into ${bSiteRackId_2}
#    Then extract value $.racks[2].id and insert into ${bSiteRackId_3}
#    Then extract value $.racks[3].id and insert into ${bSiteRackId_4}
#    Then extract value $.racks[4].id and insert into ${bSiteRackId_5}
#    Then extract value $.racks[5].id and insert into ${bSiteRackId_6}
#    Then extract value $.racks[6].id and insert into ${bSiteRackId_7}

#  @regression @working @test
#  Scenario: Adding DER solar cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 9cab06ee-70d7-4223-b46c-2bf252296f44 |
#      | battery_store_name | test_123_solar_site  |
#      | store_type         | SOLAR                   |
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK
#
#  @regression @working @test
#  Scenario: Adding DER fuel cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 9cab06ee-70d7-4223-b46c-2bf252296f44 |
#      | battery_store_name | test_123_fuelcell      |
#      | store_type         | FUEL-CELL                 |
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK

  @regression
  Scenario: Validate rate-service schedules API endpoint
    Given http-client "rate_adapter"
    When sends GET ${sUrl}
    Then receives status 200 OK

  @regression
  Scenario: Validate rate-service schedules by code API endpoint
    Given http-client "rate_adapter"
    When sends GET ${sUrl}/${ratCde}
    Then receives status 200 OK

  @regression
  Scenario: Validate topo-service utilities
    Given http-client "rate_adapter"
    When sends GET ${utlUrl}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by tariff id only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${tString}=${tarId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by utility id only
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${utlString}=${utilId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code & utility id
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${utlString}=${utilId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code & tariff id
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${tString}=${tarId}
    Then receives status 200 OK

  @regression @querystring
  Scenario: Validate rate-service tarrif by zip code, utility & tariff id's
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${zString}=${zipCde}&${utlString}=${utilId}&${tString}=${tarId}
    Then receives status 200 OK

  @regression @negative @bug @parispostalcode
  Scenario: Validate rate-service tarrif w/ zipcode querystring >> currently passing w/ 200 >> expect 400/404
    Given http-client "rate_adapter"
    Then sends GET ${tarUrl}?${zString}=${longStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${zString}=${textStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${zString}=${charStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${zString}=${charStr2}
    Then receives status 200
    Then sends GET ${tarUrl}?${zString}=75008
    Then receives status 200

  @regression @negative @bug @eufuelcelltariff
  Scenario: Validate rate-service tarrif w/ tariffid querystring >> currently passing w/ 200 >> expect 400/404
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${tString}=${longStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${tString}=${textStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${tString}=${charStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${tString}=${charStr2}
    Then receives status 200
    Then sends GET ${tarUrl}?${tString}=8501620030
    Then receives status 200

  @regression @negative @bug @mockFRcode
  Scenario: Validate rate-service tarrif w/ utilityId querystring >> currently passing w/ 200 >> expect 400/404
    Given http-client "rate_adapter"
    When sends GET ${tarUrl}?${utlString}=${longStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${utlString}=${textStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${utlString}=${charStr}
    Then receives status 200
    Then sends GET ${tarUrl}?${utlString}=${charStr2}
    Then receives status 200
    Then sends GET ${tarUrl}?${utlString}=FRA123456
    Then receives status 200

  @regression @expected_failure @negative
  Scenario: V: rate-service tarrif w/ querystring entries >> negative case >> throw 500
    Given http-client "rate_adapter"
    Then sends GET ${tarUrl}?${zString}=${ise500}
    Then receives status 500 INTERNAL_SERVER_ERROR
    Then sends GET ${tarUrl}?${tString}=${ise500}
    Then receives status 500 INTERNAL_SERVER_ERROR
    Then sends GET ${tarUrl}?${utlString}=${ise500}
    Then receives status 500

#  @regression @test @needstesting
#  Scenario: Add/Update a rate schedule
#    Given http-client "rate_adapter"
#    And Accept: application/json
#    And Content-Type: application/json
#    And Header Authorization: ${bAuth}
#    And Payload:
#    """
#    {
#      "name":"Town of Ruston",
#      "lseId":1043,
#      "shortName":"TORuston"
#    }
#    """
#    When sends PUT ${sUrl}/${tariffCde}
#    Then receives status 200 OK

#  @test
#  Scenario: V: test
#    Given http-client "rate_adapter"
#    Given variables
#      | flavDesignate3  | Metering%20Service%20Charge  |
#      | flav1  | High%20Tension     |
#      | requestFlavor | ${flav1}-${flavDesignate3} |
#      | mTarId        | 3161243                    |
#      | raW    | /raw    |
#    When sends GET ${tarUrl}/${mTarId}${raW}
#    Then receives status 200 OK
#    When sends GET ${tarUrl}/${tarId}${uRate}
#    Then receives status 200 OK
#    When sends GET ${tarUrl}/${tarId}${cfgUrl}
#    Then receives status 200 OK
#    When sends GET ${tarUrl}/${charStr2}/${requestFlavor}
#    Then receives status 200 OK

#  @test
#  Scenario: Slacker Test
#    Given: http-client "slack_adapter"
#    Given variables
#      | slackVar  | T1PSSUCD8/BCU3D3R0A/guOPGaNCiPcoAsd8kpXMvNFx  |
#    And Content-Type: application/json
#    And Payload:
#    """
#      { "text":"Sent from cucumber" }
#    """
#    Then sends POST /services/${slackVar}
#    Then receives status 200 OK
  @regression
  Scenario: V: rate-tariffs-meta
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${metUrl}/${tarCde}
    Then validate $.code is ${tarCde}
    Then validate $.deleted is false
    Then validate $.tariffType is Delivery
    Then validate $.utilityName is Pacific Gas & Electric Co
    Then validate $.name is PGE E19
    Then receives status 200 OK

  @regression
  Scenario: V: rate-tariffs-dayahead --> need dateTime,start,end qstring
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${dayUrl}/${tarCde}
    Then validate $[0].code is ${tarCde}
    Then validate $[0].interval is PT1H
    Then receives status 200 OK

  @regression
  Scenario: V: rate-tariffs-dayahead-missingdays --> need zone,start,end qstring
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=UTC
    Then receives status 200 OK

  @regression @expected_failure @den2018-3768
  Scenario: V: rate-tariffs-dayahead-missingdays --> need zone,start,end qstring
    Given http-client "rate_adapter"
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-10-10&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=31-01-2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-01-10T00:00:01Z&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=01.01.2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?start=2018-09-10&zone=UTC
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=111&start=01.01.2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=4095D&start=01.01.2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=GOMER&start=01.01.2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=g0M3Rr&start=01.01.2018&zone=EST
    Then receives status 400 BAD_REQUEST
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=XXX
    Then receives status 500 INTERNAL_SERVER_ERROR
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=%5E
    Then receives status 500 INTERNAL_SERVER_ERROR
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=utc
    Then receives status 500 INTERNAL_SERVER_ERROR
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=-25:00
    Then receives status 500 INTERNAL_SERVER_ERROR
    When sends GET ${tarUrl}${dayUrl}/missing-days?code=E19&start=2018-09-10&zone=-01:02.025
    Then receives status 500 INTERNAL_SERVER_ERROR
