Feature:  info endpoint tests

  Background:
    Given variables
      | sStatus | UP |
      | statUp  | UP |
      | statDn  | DOWN |
      | svcState | /health |
      | svcInfo  | /info   |
      | cfgUrl   | /config |
      | qString  | group_id |

  @dev @soak
  Scenario: Validate rate-service API
    Given http-client "rate_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate stats-service API
    Given http-client "stats_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate stats-intercept-service API
    Given http-client "si_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate oe-service API
    Given http-client "oe_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate event-recorder API
    Given http-client "eventrecorder_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate vpp-manager-service API
    Given http-client "vpp_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate forcast-service API
    Given http-client "forcast_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate tornado-service API
    Given http-client "tornado_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate dr-rates API
    Given http-client "dr_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate day-ahead API
    Given http-client "dayahead_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate topography API
    Given http-client "topo_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate carnac API
    Given http-client "carnac_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate rate-capture API
    Given http-client "capture_adapter"
    When sends GET ${svcInfo}
    Then receives status 200 OK

#  @dev @soak @notworking
#  Scenario: Validate forecastms API
#    Given http-client "forecastms_adapter"
#    When sends GET ${svcInfo}
#    Then receives status 200 OK
