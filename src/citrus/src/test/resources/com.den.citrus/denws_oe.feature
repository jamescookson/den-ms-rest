Feature: Validation of oe REST endpoints

  Background:
    Given variables
      | sStatus  | UP       |
      | statUp   | UP       |
      | statDn   | DOWN     |
      | cfgUrl   | /config  |
      | gString  | group_id |

  @regression
  Scenario: Validate oe-service /config API endpoint
    Given http-client "oe_adapter"
    When sends GET ${cfgUrl}
    Then receives status 200 OK

  @in_dev
  Scenario: Validate specific entries from a known config
    Given http-client "oe_adapter"
    Given variables
      | groupID     | 47bea4b8-bf28-4b91-bea3-88d533200bb3 |
      | b_battName  | Enersys                              |
      | grpRate     | CSTE19                               |
      | supRate     | null                                 |
      | timeZone    | UTC                                  |
      | l_ldType    | consumptive-load                     |
      | l_mtType    | building                             |
      | c_llv       | lower_limit_violation                |
      | c_bec       | battery_energy_change                |
      | startTime   | 2015-12-31T00:00-08:00               |
      | endTime     | 2016-01-31T23:59:59-08:00            |
    When sends GET ${cfgUrl}?${gString}=${groupID}
    Then validate $[0].siteId is ${groupID}
    Then validate $[0].deliveryRate is ${grpRate}
    Then validate $[0].timeZone is ${timeZone}
    Then validate $[0].battery.name is ${b_battName}
    Then validate $[0].loads[0].loadType is ${l_ldType)
    Then validate $[0].loads[0].meterType is ${l_mtType)
    Then validate $[0].costs[0] is ${c_llv)
    Then validate $[0].costs[1] is ${c_bec}
    Then validate $[0].date-range.start is ${startTime)
    Then validate $[0].date-range.end is ${endTime)
    Then receives status 200 OK

#  For the sake of convention, when an item is within a nested json path
#  I will use first letter of section name followed by '_'

#  Given http=client "oe_adapter"
#  Given Payload:
#  """
#  {
#     "power":
#     {
#        "class":"com.den.grid.value.PowerValue",
#        "value":${TotalPowerValue},"defaultUnit":"kW"
#     },
#     "duration":
#     {
#        "class":"com.den.grid.time.TimeDuration",
#        "value":${DurationMillis},"defaultUnit":"ms"
#     }
#  }
#  """
#  And Content-Type: application/json
