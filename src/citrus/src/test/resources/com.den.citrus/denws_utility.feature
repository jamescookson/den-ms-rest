Feature: V: Utility-Related REST Calls

  Background: Requires that a site has been created and battery DER's added to it.
  Scenario: Adding utility to DER by its ID
    Given http-client "denws_adapter"
    Given variables
      | bSiteId  | be7f4250-5165-484c-9596-26fbc9782fc7 |
      | utilName | BogusUtility |
      | site_name| ${site_name}          |
      | bDvcId   | be7f4250-5165-484c-9596-26fbc9782fc7 |
      | bSID     | be7f4250-5165-484c-9596-26fbc9782fc7 |
      | timeZone | UTC       |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "id":${bSiteId},
      "name":${site_name},
      "SID":${bSID},
      "deviceiD":${bDvcId},
      "timezone":$timeZone},
      "utility":${utilName},
      "vppSiteConfiguration":0.8, (not sure about this #)
      "tags":null
    }
    """
    When sends PUT /rest/sites/${bSiteId}
    Then receives status 200 OK

  @in_dev
  Scenario: Adding a rate structure
    Given http-client "denws_adapter"
    Given variables
      | bSiteId  | be7f4250-5165-484c-9596-26fbc9782fc7 |
      | ratBefor | name_here |
      | ratAfter | name_here |
      | supBefor | name_here |
      | supAfter | name_here |
    And Content-Type: application/json
    And Header Authorization: ${bAuth}
    And Payload:
    """
    {
      "deliveryAfter":${ratAfter},
      "deliveryBefore":${ratBefor},
      "supplyBefore":${supBefor},
      "supplyAfter":{supAfter}
    }
    """
    When sends PUT /rest/sites/${bSiteId}/rates
    Then receives status 200 OK
