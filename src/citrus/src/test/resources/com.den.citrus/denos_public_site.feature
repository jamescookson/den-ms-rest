Feature:  denws site/account specific tests

  Background:
    Given variables
      | uRest   | /rest |
      | uSite   | /sites  |
      | uStore  | /stores |
      | uRate   | /rates  |
      | rstUrl  | /rest   |
      | uAcct   | /accounts |
      | vUrl    | /version  |
      | qName   | James     |
      | qeBatSy | Enersys   |
      | qeStrTp | DEN-STORE |
      | dvVers  | develop-2127 |
      | qeMaxDOD  | 65         |
      | qeSiteNm  | TestSite1  |
      | qeStorNm  | Battery1   |
      | qeRak1Nm  | ESS Rack 1 |
      | qeRak2Nm  | ESS Rack 2 |
      | qeAvailPower  | 105    |
      | qeNumBattery  | 40     |
      | qeNamBatEner  | 203.52 |
      | newLocName    | qe_automate_site_1 |
      | qeStore | 768b8574-d513-4b48-a5cc-1401808078ea  |
      | qeDvcId | 672cb9a6-b742-4c95-8388-648c9358f771  |
      | qeStrId | 88b2c7a1-435c-45b2-8a74-20653aac221a  |
      | qeRak1  | a6d41e60-680e-4dcd-8ce0-d04d69d7add4  |
      | qeRak2  | 952e8e00-7b34-44ca-ba24-fd0b09fe3a24  |
      | qeStoreSiteId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
      | dev_bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |
      | soak_bAuth| Basic UUFhdXRvbWF0aW9uOmY0YzBjMmU3LWI0NzgtNDAzMi05YTVhLWUxMjUyZDJhMWI5Nw== |

  @dev @site_specific
  Scenario: Validate denws /version API
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 200 OK

  @dev @site_specific @negative
  Scenario: V: Auth attempt w/ SOAK creds fails
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 401 UNAUTHORIZED

  @soak @site_specific
  Scenario: Validate denws /version API
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 200 OK

  @soak @site_specific
  Scenario: V: Auth attempt w/ DEV creds fails
    Given http-client "denws_adapter"
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${uRest}${vUrl}
    Then receives status 401 UNAUTHORIZED

  @site_specific
  Scenario: V: sales-modeling submission state - finished submission
    Given http-client "denws_adapter"
    Given variables
      | siteId |  be7f4250-5165-484c-9596-26fbc9782fc7 |
      | fileName | Boston_601_Take2_5min_Jan16.csv     |
      | salesSt  | finished                            |
      | count    | 8928                                |
    And Content-Type: application/json
    And Header Authorization: ${dev_bAuth}
    When sends GET ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
    Then validate $[0].fileName is ${fileName}
    Then validate $[0].status is ${salesSt}
    Then validate $[0].count is ${count}
    Then receives status 200 OK

  @site_specific
  Scenario: V: sales-modeling submission state - finished submission
    Given http-client "denws_adapter"
    Given variables
      | siteId |  1048362d-34f4-4db9-8c3d-7fc6e40bcc81 |
      | fileName | Fletcher Jones Sales Modeling Demo.csv     |
      | salesSt  | finished                            |
      | count    | 8835                                |
    And Content-Type: application/json
    And Header Authorization: ${soak_bAuth}
    When sends GET ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
    Then validate $[0].fileName is ${fileName}
    Then validate $[0].status is ${salesSt}
    Then validate $[0].count is ${count}
    Then receives status 200 OK

#  @soak @site_specific
#  Scenario: V: sales-modeling submission state - finished submission
#    Given http-client "denws_adapter"
#    Given variables
#      | siteId |  be7f4250-5165-484c-9596-26fbc9782fc7 |
#      | fileName | Boston_601_Take2_5min_Jan16.csv     |
#      | salesSt  | finished                            |
#      | count    | 8928                                |
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    When sends GET ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
#    Then validate $[0].fileName is ${fileName}
#    Then validate $[0].status is ${salesSt}
#    Then validate $[0].count is ${count}
#    Then receives status 200 OK
#
#  @soak @addDER @test
#  Scenario: Adding a DER location (withing DEN.OS Cloud >> Demand Energy >> QE)
#    Given http-client "denws_adapter"
#    Given variables
#      | soak_qeStore |  e4fec7d5-f6fa-46a3-b5c1-2596092056f7 |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    When sends POST ${rstUrl}${uAcct}/${soak_qeStore}${uSite}/${newLocName}
#    Then receives status 200 OK
#
#  @soak @addingDERrack @test
#  Scenario: Adding DER battery store with 2 racks to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 1048362d-34f4-4db9-8c3d-7fc6e40bcc81 |
#      | battery_store_name | soak_battery_site  |
#      | store_type         | DEN-STORE                 |
#      | battery_rack1_name | soak_rack1     |
#      | battery_rack2_name | soak_rack2     |
#      | batterySystem      | Enersys                   |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":105,
#        "racks":[
#          {"id":null,"name":"${battery_rack1_name}","rackNumber":1},
#          {"id":null,"name":"${battery_rack2_name}","rackNumber":2}
#        ],
#        "maxDOD":65,
#        "batterySystem":${batterySystem}
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK
#
#  @soak @addingDERSolar  @site_sapecific
#  Scenario: Adding DER solar cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 1048362d-34f4-4db9-8c3d-7fc6e40bcc81 |
#      | battery_store_name | soak_solar_site  |
#      | store_type         | SOLAR                   |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK
#
#  @soak @addinghDERfuelcell @site_sapecific
#  Scenario: Adding DER fuel cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | 1048362d-34f4-4db9-8c3d-7fc6e40bcc81 |
#      | battery_store_name | soak_fuelcell      |
#      | store_type         | FUEL-CELL                 |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK


#  @dev @notworking
#  Scenario: Validate denws QE location & specific details
#    Given http-client "denws_adapter"
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    When sends GET ${uRest}${uAcct}/${qeStore}
#    Then validate $.name is ${qName}
#    Then validate $.id is ${qeStore}
#    Then validate $.sites[1].timezone is UTC
#    Then validate $.sites[1].deviceId is ${qeDvcId}
#    Then validate $.sites[1].name is ${qeSiteNm}
#    Then validate $.sites[1].availablePower is ${qeAvailPower}
#    Then validate $.sites[1].stores[0].id is ${qeStrId}
#    Then validate $.sites[1].stores[0].type is ${qeStrTp}
#    Then validate $.sites[1].stores[0].name is ${qeStorNm}
#    Then validate $.sites[1].stores[0].siteId is ${qeStoreSiteId}
#    Then validate $.sites[1].stores[0].maxDOD is ${qeMaxDOD}
#    Then validate $.sites[1].stores[0].availablePower is ${qeAvailPower}
#    Then validate $.sites[1].stores[0].nameplateBatteryEnergy is ${qeNamBatEner}
#    Then validate $.sites[1].stores[0].racks[0].id is ${qeRak1}
#    Then validate $.sites[1].stores[0].racks[0].siteId is ${qeStoreSiteId}
#    Then validate $.sites[1].stores[0].racks[0].name is ${qeRak1Nm}
#    Then validate $.sites[1].stores[0].racks[0].numBatteries is ${qeNumBattery}
#    Then validate $.sites[1].stores[0].racks[1].id is ${qeRak2}
#    Then validate $.sites[1].stores[0].racks[1].siteId is ${qeStoreSiteId}
#    Then validate $.sites[1].stores[0].racks[1].name is ${qeRak2Nm}
#    Then validate $.sites[1].stores[0].racks[1].numBatteries is ${qeNumBattery}
#    Then receives status 200 OK
#  @dev @notworking
#  Scenario: uploading data file
#    Given http-client "denws_adapter"
#    Given variables
#      | siteId |  be7f4250-5165-484c-9596-26fbc9782fc7 |
#      | filename | Upload_B601_5min_Jan16.csv |
#      | startDt  | 2016-01-01T05:00:00.000Z   |
#      | endDt    | 2016-02-01T05:00:00.000Z   |
#      | interVal | Payload:                   |
#    And Content-Type: multipart/form-data
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#    {
#      "fileName":"${filename}",
#      "start":"${startDt}",
#      "end":"${endDt}"
#      "interval":"${interVal}"
#    }
#    """
#    When sends POST ${uRest}${uSite}/${siteId}/modeling/jobs/interval-data/status
#    Then receives status 200 OK

#  Scenario Outline: Creating item necessary to setup site for SalesOps to show off to potential customers.
#    Then sets variable <> to new value <>
#    Need: figure out how to insert value from path into variable
#    Futrure Step Definition for  extraction
#    Then extract value <location reference> and insert into variable <variable name>
#    CREATED siteId be7f4250-5165-484c-9596-26fbc9782fc7 using ^^ & used for succeeding cases below

#  @regression @working
#  Scenario: Adding a DER location (withing DEN.OS Cloud >> Demand Energy >> QE)
#    Given http-client "denws_adapter"
#    Given variables
#      | qeStore |  be7f4250-5165-484c-9596-26fbc9782fc7 |
#    And Content-Type: application/json
#    And Header Authorization: ${soak_bAuth}
#    When sends POST ${rstUrl}${uAcct}/${qeStore}${uSite}/${newLocName}
#    Then receives status 200 OK
#
#  @dev @working
#  Scenario: Adding DER battery store with 2 racks to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | be7f4250-5165-484c-9596-26fbc9782fc7 | << unnecessary once injected into var on the fly
#      | battery_store_name | qa_automate_battery_site  |
#      | store_type         | DEN-STORE                 |
#      | battery_rack1_name | qa_automate_bat_rack1     |
#      | battery_rack2_name | qa_automate_bat_rack2     |
#      | batterySystem      | Enersys                   |
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":105,
#        "racks":[
#          {"id":null,"name":"DER_rack1","rackNumber":1},
#          {"id":null,"name":"DER_rack2","rackNumber":2},
#          {"id":null,"name":"DER_rack3","rackNumber":3},
#          {"id":null,"name":"DER_rack4","rackNumber":4},
#          {"id":null,"name":"DER_rack5","rackNumber":5},
#          {"id":null,"name":"DER_rack6","rackNumber":6},
#          {"id":null,"name":"DER_rack7","rackNumber":7}
#        ],
#        "maxDOD":65,
#        "batterySystem":${batterySystem}
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK
#
#  @dev @working
#  Scenario: Adding DER solar cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | be7f4250-5165-484c-9596-26fbc9782fc7 |
#      | battery_store_name | qa_automate_solar_site  |
#      | store_type         | SOLAR                   |
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK
#
#  @regression @working
#  Scenario: Adding DER fuel cell to a location
#    Given http-client "denws_adapter"
#    Given variables
#      | bSiteId | be7f4250-5165-484c-9596-26fbc9782fc7 |
#      | battery_store_name | qa_automate_fuelcell      |
#      | store_type         | FUEL-CELL                 |
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#      {
#        "id":null,
#        "SID":null,
#        "name":${battery_store_name},
#        "type":${store_type},
#        "availablePower":null,
#        "racks":[],
#      }
#    """
#    When sends POST ${rstUrl}${uSite}/${bSiteId}${uStore}
#    Then receives status 200 OK

#  @regression @test @needstesting @denws
#  Scenario: Add a utility for a customer via DENWS site
#    Given http-client "denws_adapter"
#    Given variables
#      | siteId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
#      | sSID   | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
#      | dvcId | 47bea4b8-bf28-4b91-bea3-88d533200bb3  |
#      | timezone | UTC  |
#      | name | TOGigHarbor  |
#      | utilName | Town of Gig Harbor  |
#    And Accept: application/json
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    And Payload:
#    """
#    {
#      "id":"${siteId}%",
#      "name":"${name}%",
#      "SID":"${sSID}%",
#      "deviceiD":"${dvcId}%",
#      "timezone":"${timezone}%",
#      "utility":"${utilName}%",
#      "address":null,
#      "vppSiteConfiguration":0.8,
#      "tags":null
#    }
#    """
#    When sends PUT ${uRest}${uSite}/${siteId}
#    Then receives status 200 OK

#  @needstesting @regression
#  Scenario: V: sales-modeling submission - fixed values - finished submission
#    Given http-client "denws_adapter"
#    And Content-Type: application/json
#    And Header Authorization: ${dev_bAuth}
#    When sends GET ${uRest}${uSite}/47bea4b8-bf28-4b91-bea3-88d533200bb3/modeling/jobs/interval-data/status
#    Then validate $[0].fileName is Boston_601_Take2_5min_Jan16.csv
#    Then validate $[0].status is finished
#    Then receives status 200 OK
