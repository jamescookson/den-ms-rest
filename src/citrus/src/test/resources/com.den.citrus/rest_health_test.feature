Feature:  health endpoint tests

  Background:
    Given variables
      | sStatus | UP |
      | statUp  | UP |
      | statDn  | DOWN |
      | svcState | /health |
      | svcInfo  | /info   |
      | cfgUrl   | /config |
      | qString  | group_id |
      | rstUrl   | /rest    |
      | vUrl     | /version |
      | dvVers   | develop-2127 |
      | bAuth    | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |

  @dev @soak @test
  Scenario: Validate rate-service API
    Given variable "passEnv" is "dev"
    Given http-client "rate_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate stats-service API
    Given http-client "stats_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then validate $.diskSpace.status is ${sStatus}
    Then validate $.configServer.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak @test
  Scenario: Validate stats-intercept-service API
    Given http-client "si_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate oe-service API
    Given http-client "oe_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then validate $.diskSpace.status is ${sStatus}
    Then validate $.configServer.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate vpp-manager-service API
    Given http-client "vpp_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate forcast-service API
    Given http-client "forcast_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then validate $.diskSpace.status is ${sStatus}
    Then validate $.db.status is ${sStatus}
    Then validate $.configServer.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate carnac API
    Given http-client "carnac_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate tornado API
    Given http-client "tornado_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate day-ahead API
    Given http-client "dayahead_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate topography API
    Given http-client "topo_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate dr-rates API
    Given http-client "dr_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate event-recorder API
    Given http-client "eventrecorder_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate forecastms API
    Given http-client "forecastms_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

  @dev @soak
  Scenario: Validate rate-capture API
    Given http-client "capture_adapter"
    When sends GET ${svcState}
    Then validate $.status is ${sStatus}
    Then receives status 200 OK

#  For the sake of convention, when an item is within a nested json path
#  I will use first letter of section name followed by '_'
#  Scenario: Validate specific entries from a known config
#    Given http-client "oe_adapter"
#    Given variables
#      | groupID     | a5084fc4-dfc6-4a5c-af23-8fc84ae9dc51 |
#      | b_battName  | Enersys                              |
#      | grpRate     | SC12RateII                           |
#      | supRate     | flat08                               |
#      | timeZone    | UTC                                  |
#      | l_ldType    | consumptive-load                     |
#      | l_mtType    | building                             |
#      | c_llv       | lower_limit_violation                |
#      | c_bec       | battery_energy_change                |
#      | startTime   | 2015-12-31T00:00-08:00               |
#      | endTime     | 2016-01-31T23:59:59-08:00            |
#    When sends GET ${cfgUrl}?${qString}=${groupID}
#    Then validate $[0].siteId is ${groupID}
#    Then validate $[0].deliveryRate is ${grpRate}
#    Then validate $[0].supplyRate is ${supRate}
#    Then validate $[0].timeZone is ${timeZone}
#    Then validate $[0].battery.name is ${b_battName}
#    Then validate $[0].loads[0].loadType is ${l_ldType)
#    Then validate $[0].loads[0].meterType is ${l_mtType)
#    Then validate $[0].costs[0] is ${c_llv)
#    Then validate $[0].costs[2] is ${c_bec}
#    Then validate $[0].date-range.start is ${startTime)
#    Then validate $[0].date-range.end is ${endTime)
#    Then receives status 200 OK