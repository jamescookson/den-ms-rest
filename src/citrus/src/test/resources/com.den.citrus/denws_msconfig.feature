Feature: V: MSConfig REST Calls

  Background:
    Given variables
      | env1       | dev              |
      | env2       | soak             |
      | bAuth   | Basic UUFhdXRvbWF0aW9uOjFmN2M4NjkyLTk5NDEtNDhjMi05MTE2LWMwNDdmODhlMDhkMA== |

  @regression
  Scenario:  .. for topology svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName  | topology-service |
      | apiFile    | file:///search/${servName}/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for dr-rates svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | dr-rates |
      | apiFile    | file:///search/${servName}/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for stats svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName  | stats-service |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for rate svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName  | rate-service |
      | apiFile    | file:///search/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for tornado svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | tornado |
      | apiFile    | file:///search/${servName}/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for carnac svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | carnac |
      | apiFile    | file:///search/${servName}/${env1}/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is file:///search/${servName}/application.yml
    Then receives status 200 OK

  @regression
  Scenario:  .. for forecasems svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | forecastms |
      | apiFile    | file:///search/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for oe-service svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | oe-service |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for stats-intercept svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | stats-intercept |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for vpp-manager svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | vpp-manager |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for forecast-service svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | forecast-service |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is file:///search/${servName}/${env1}/application.properties
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for day-ahead svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | day-ahead |
      | apiFile    | file:///search/${servName}/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for event-recorder svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | event-recorder |
      | apiFile    | file:///search/${servName}/application.properties |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK

  @regression
  Scenario:  .. for dws-diagnostic svc
    Given http-client "msconfig_adapter"
    Given variables
      | servName   | dws-diagnostic |
      | apiFile    | file:///search/application.yml |
    And Header Authorization: ${bAuth}
    When sends GET /${servName}/${env1}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env1}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
    When sends GET /${servName}/${env2}
    Then validate $.name is ${servName}
    Then validate $.profiles[0] is ${env2}
    Then validate $.propertySources[0].name is ${apiFile}
    Then receives status 200 OK
