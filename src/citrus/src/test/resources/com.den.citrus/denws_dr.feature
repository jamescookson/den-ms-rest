Feature: V: dr REST endpoints

  Background:
    Given variables
      | sStatus  | UP       |
      | statUp   | UP       |
      | statDn   | DOWN     |
      | sUrl     | /schedules |
      | pgString | program-name=|
      | pgName   | Arabesque    |
      | fmString | format=      |
      | fmType   | flattened-rate|
      | frStr    | from=         |
      | toStr    | to=           |
      | intStr   | 15            |
      | tS    | 1538776564000    |
      | tS2   | ${tS}+900000     |
      | tS3   | ${tS}+1800000    |
      | frDate   | 2018-10-05T21:56:04|
      | toDate   | 2018-11-05T23:56:04|
      | schId    | 9bc3d637-46ca-426f-9478-ab4e0cc9dbf4|

  @regression
  Scenario: API: dr-rates available schedules
    Given http-client "dr_adapter"
    When sends GET ${sUrl}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by ID
    Given http-client "dr_adapter"
    When sends GET ${sUrl}/${schId}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by program name
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}
    Then validate $[0].drScheduleId is ${schId}
    Then validate $[0].programName is ${pgName}
    Then validate $[0].startDateTimeUtc is ${frDate}
    Then validate $[0].endDateTimeUtc is ${toDate}
    Then receives status 200 OK

  @regression
  Scenario: API: flattened dr-rates by ID
    Given http-client "dr_adapter"
    When sends GET ${sUrl}/${schId}?${fmString}${fmType}
    Then validate $.drScheduleId is ${schId}
    Then validate $.programName is ${pgName}
    Then validate $.intervalMinutes is ${intStr}
    Then validate $.timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK
    #Then validate $.timeStamps[1].timeStamp is ${tS2}
    #Then validate $.timeStamps[2].timeStamp is ${tS3}

  @regression
  Scenario: API: dr-rates by name and scheduled range
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${toDate}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name and scheduled range (missing TO)
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${frDate}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name and scheduled range (missing FROM)
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${toStr}${toDate}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name and scheduled range (custom FROM)
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2000-10-05T21:56:04&${toStr}${toDate}
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name and scheduled range (custom TO)
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}2024-10-05T21:56:04
    Then receives status 200 OK

  @regression
  Scenario: API: dr-rates by name, scheduled range AND flattened output
    Given http-client "dr_adapter"
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then validate $[0].drScheduleId is ${schId}
    Then validate $[0].programName is ${pgName}
    Then validate $[0].intervalMinutes is ${intStr}
    Then validate $[0].timeStamps[0].timeStamp is ${tS}
    Then receives status 200 OK

#  For the sake of convention, when an item is within a nested json path
#  I will use first letter of section name followed by '_'

#  Given http=client "oe_adapter"
#  Given Payload:
#  """
#  {
#     "power":
#     {
#        "class":"com.den.grid.value.PowerValue",
#        "value":${TotalPowerValue},"defaultUnit":"kW"
#     },
#     "duration":
#     {
#        "class":"com.den.grid.time.TimeDuration",
#        "value":${DurationMillis},"defaultUnit":"ms"
#     }
#  }
#  """
#  And Content-Type: application/json


  @expected_failure @regression
  Scenario: expected failure cases (either 400-404-XXX)
    Given http-client "dr_adapter"
    Given variables
      | badChr | %7B%5B%E2%80%A6?%5D%7D |
      | badTxt | hello_dolly            |
      | ise500   | %21%7E%7B%40%88%2E%23%7D%5F%25%9C%A7%24 |
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badTxt}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}${badChr}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${badChr}&${toStr}${toDate}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}${badTxt}&${toStr}${toDate}
    Then receives status 400 BAD_REQUEST
    When sends GET /${badTxt}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET /${badChr}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET /${ise500}${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET /${badChr}?${pgString}${pgName}
    Then receives status 404 NOT_FOUND
    When sends GET /${badTxt}?${pgString}${pgName}
    Then receives status 404 NOT_FOUND
    When sends GET ${sUrl}/${badChr}?${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}/${badTxt}?${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}/${ise500}?${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET /${badChr}/${schId}?${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET /${badTxt}/${schId}?${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET ${sUrl}/${schId}?${fmString}${badChr}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}/${schId}?${fmString}${badTxt}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}/${schId}?${fmString}
    Then receives status 400 BAD_REQUEST
    When sends GET //${schId}?${fmString}${fmType}
    Then receives status 404 NOT_FOUND
    When sends GET ${sUrl}/?${fmString}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2020-01-01T01:01:01+000Z&${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}01/01/2020&${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2000-01-01T01:01:01+000Z&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then receives status 400 BAD_REQUEST
    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}01/01/2000&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
    Then receives status 400 BAD_REQUEST


#    When sends GET ${sUrl}?${pgString}${pgName}&${frStr}2020-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType}
#    Then receives status 404 NOT_FOUND

#  @failures @test
#  Scenario: expected failure cases (either 400-404-XXX)
#    Given http-client "dr_adapter"
#      | type | url | code | message |
#      | GET | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}hello_dolly | 400 | BAD_REQUEST |
#      | GET | ${sUrl}?${pgString}${pgName}&${frStr}${frDate}&${toStr}%7B%5B%5D%7D? | 400 | BAD_REQUEST |
#      | GET | ${sUrl}?${pgString}${pgName}&${frStr}%7B%5B%5D%7D?&${toStr}${toDate} | 400 | BAD_REQUEST |
#      | GET | ${sUrl}?${pgString}${pgName}&${frStr}hello_dolly&${toStr}${toDate} | 400 | BAD_REQUEST |
#      | GET | /hello_dolly?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST |
#      | GET | /%7B%5B%E2%80%A6?%5D%7D?${pgString}${pgName}&${frStr}2010-10-05T21:56:04&${toStr}2019-11-05T23:56:04&${fmString}${fmType} | 400 | BAD_REQUEST |
#    When sends <type> request to <url>
#    Then they should receive a <code> response with the message <message>
